# coding: utf-8
import json
from scrapy import Request
from scrapy.spiders import Spider
from crawler.items import LoanItem, PartnerItem, TeamItem, MemberItem, LoanPartnerItem, LoanTeamItem, LoanMemberItem, TeamMemberItem


class KivaSpider(Spider):
    """
    贷款抓取
    """
    name = 'kiva.org'
    allowed_domains = ['kiva.org', 'api.kivaws.org']
    start_urls = ['https://www.kiva.org']

    def parse(self, response):
        """
        抓取贷款
        """
        json_response = json.loads(response.body_as_unicode())

        pages = json_response['paging']

        if pages['pages'] > 0:
            for page in xrange(pages['pages'] - 20, pages['pages'] + 1):
                url = 'http://api.kivaws.org/v1/loans/search.json?sort_by=newest&status=fundraising&page=%s' % page
                yield Request(url=url, callback=self.parse2)

    def parse2(self, response):
        """
        取得贷款列表
        """
        json_response = json.loads(response.body_as_unicode())

        for loan in json_response['loans']:
            yield self.make_loan(loan['id'])

    def make_loan(self, loan_id):
        url = 'http://www.kiva.org/lend/%s' % loan_id
        req = Request(url=url, callback=self.parse_loan)
        return req

    def parse_loan(self, response):
        """
        通过html取得贷款更多信息
        """
        item = response.meta['loan']
        api_text = response.xpath('//section[@id="businessProfile"]/div/script/text()').extract_first().replace('var Loan_LoanFigureSubView = ', '')[:-1]
        api = json.loads(api_text)

        item['id'] = api['loan']['id']
        item['loan_name'] = api['loan']['name']
        if len(api['loan']['borrowers']) > 1:
            item['group_user_names'] = ',' % ','.join([borrower['firstName'] for borrower in api['loan']['borrowers']])
        item['country'] = api['loan']['location']['country']['name']
        item['industry'] = api['loan']['sector']['name']
        item['industry2'] = api['loan']['activity']['name']
        item['loan_overview'] = api['loan']['description']['en'].replace('<br />', '\r\n')
        item['loan_amount'] = api['loan']['loanAmount']['amount']
        item['fundraising_status'] = api['loan']['status']
        item['repayment_term'] = response.xpath('//div[@id="loanSummary"]/dl/dd[1]/text()').extract_first()
        item['repayment_schedule'] = response.xpath('//div[@id="loanSummary"]/dl/dd[2]/text()').extract_first()
        item['pre_disbursed'] = response.xpath('//div[@id="loanSummary"]/dl/dd[3]/text()').extract_first()
        item['listed'] = response.xpath('//div[@id="loanSummary"]/dl/dd[4]/text()').extract_first()
        item['currency_exchange_loss'] = response.xpath('//div[@id="loanSummary"]/dl/dd[5]/text()').extract_first()
        item['additional_information'] = '\r\n'.join(response.xpath('//section[@id="additionalLoanInformation"]/*/text()').extract())
        translalor = response.xpath('//div[@class="byLine"]/div[2]/text()').extract()
        if len(translalor) == 2:
            item['translalor'] = translalor[1]
        item['original_language'] = response.xpath('//div[@class="byLine"]/div[1]/text()').extract_first()
        if 'Translated from ' in item['original_language']:
            item['original_language'] = item['original_language'].replace('Translated from ', '')
        item['loan_fund_status'] = response.xpath('//div[@class="currentFunding"]/span[1]/text()').extract_first()

        # 和partner关联
        partner_id = api['loan']['partnerId']
        relation = LoanPartnerItem()
        relation['loan_id'] = item['id']
        relation['partner_id'] = partner_id
        yield relation
        # 抓取partner
        yield self.make_partner(partner_id)

        # 抓取teams
        url = 'http://www.kiva.org/ajax/callLoan_TopLendingTeamSubView?&biz_id=%s&show_all_teams=true' % item['id']
        req = Request(url=url, callback=self.parse_loan_teams)
        req.meta.update({'loan': item})
        yield req

        # 抓取lender
        api2_text = response.xpath('//body/script[1]/text()').extract_first().replace('var Loan_LoanView = ', '')[:-1]
        api2 = json.loads(api2_text)
        url = 'http://www.kiva.org/ajax/callLoan_TopLendersSubView?biz_id=942704&show_all_lenders=1&lendersJSON=[%s]' % ','.join(api2['lendersJSON'])
        req = Request(url=url, callback=self.parse_loan_lenders)
        req.meta.update({'loan': item})
        yield req

        yield item

    def parse_loan_teams(self, response):
        loan = response.meta['loan']
        for team_url in response.xpath('//ul/li/article/a/@href').extract():
            team_uid = team_url.replace('http://www.kiva.org/team/', '')

            relation = LoanTeamItem()
            relation['loan_id'] = loan['id']
            relation['team_uid'] = team_uid
            yield relation

            yield self.make_team(team_uid)

    def parse_loan_lenders(self, response):
        loan = response.meta['loan']
        for lender_url in response.xpath('//ul/li/div/a/@href').extract():
            lender_uid = lender_url.replace('http://www.kiva.org/lender/', '')

            relation = LoanMemberItem()
            relation['loan_id'] = loan['id']
            relation['member_uid'] = lender_uid
            yield relation

            yield self.make_lender(lender_uid)

    def make_partner(self, partner_id):
        partner = PartnerItem()
        partner['id'] = partner_id
        url = 'http://www.kiva.org/partners/%s' % partner['id']
        req = Request(url=url, callback=self.parse_partner)
        req.meta.update({'partner': partner})
        return req

    def parse_partner(self, response):
        """
        抓取伙伴信息
        """
        item = response.meta['partner']
        item['name'] = response.xpath('//div[@class="info"]/dl/dd[1]/a/text()').extract_first()
        risk_rating = response.xpath('//div[@class="info"]/dl/dd[2]/div/@title').re('([0-9\.]+)')
        if len(risk_rating) > 0:
            item['risk_rating'] = risk_rating[0]
        else:
            item['risk_rating'] = 0
        item['time_on_kiva'] = response.xpath('//div[@class="info"]/dl/dd[3]/text()').extract_first()
        item['kiva_borrower'] = response.xpath('//div[@class="info"]/dl/dd[4]/text()').extract_first()
        item['total_loan'] = response.xpath('//div[@class="info"]/dl/dd[5]/text()').extract_first()
        item['interest_fees_are_charged'] = response.xpath('//div[@class="info"]/dl/dd[6]/text()').extract_first()
        item['average_cost_to_borrower'] = response.xpath('//div[@class="info"]/dl/dd[7]/text()').extract_first()
        item['profitability'] = response.xpath('//div[@class="info"]/dl/dd[8]/text()').extract_first()
        item['average_loan_size'] = response.xpath('//div[@class="info"]/dl/dd[9]/text()').extract_first()
        item['delinquency_rate'] = response.xpath('//div[@class="info"]/dl/dd[10]/text()').extract_first()
        item['loans_at_risk_rate'] = response.xpath('//div[@class="info"]/dl/dd[11]/text()').extract_first()
        item['default_rate'] = response.xpath('//div[@class="info"]/dl/dd[12]/text()').extract_first()
        item['currency_exchange_loss_rate'] = response.xpath('//div[@class="info"]/dl/dd[13]/text()').extract_first()
        yield item

    def make_team(self, team_uid):
        item = TeamItem()
        item.uid = team_uid
        url = 'http://www.kiva.org/team/%s' % team_uid
        req = Request(url=url, callback=self.parse_team)
        req.meta.update({'team': item})
        return req

    def parse_team(self, response):
        item = response.meta['team']
        api_text = response.xpath('//body/script[1]/text()').extract_first().replace('var Community_Team_TeamSummaryView = ', '')[:-1]
        api = json.loads(api_text)
        item['id'] = api['tid']

        url = 'https://api.kivaws.org/SirenV1/team/%s' % item['id']
        req = Request(url=url, callback=self.parse_team2)
        req.meta.update({'team': item})
        yield req

        item['name'] = response.xpath('//h2[@class="hPage"]/text()').extract_first()
        item['introdcution'] = '\r\n'.join(response.xpath('//div[@class="profileStats"]/*/text()').extract())
        item['type'] = response.xpath('//div[@class="meta"]/a/text()').extract_first()
        item['since'] = response.xpath('//div[@class="meta"]/text()').extract()[1].replace(' team since ', '')
        item['total_loan_amount'] = response.xpath('//div[@class="rightCol g4 z"]/div[2]/div[1]/text()').extract_first()
        item['total_numebr_of_member'] = response.xpath('//div[@class="rightCol g4 z"]/div[1]/div[1]/text()').extract_first()
        team_ranking = response.xpath('//div[@class="rightCol g4 z"]/section').extract()
        if len(team_ranking) > 2:
            item['team_ranking'] = '%s%s' % (team_ranking[1], team_ranking[2])
        elif len(team_ranking) > 1:
            item['team_ranking'] = team_ranking[1]
        yield item

        # 抓取lender
        url = 'http://api.kivaws.org/v1/loans/%s/lenders.json' % item['id']
        req = Request(url=url, callback=self.parse_team_lender)
        req.meta.update({'team': item})
        yield req

    def parse_team2(self, response):
        json_response = json.loads(response.body_as_unicode())
        team = response.meta['team']

        team['name'] = json_response['properties']['name']
        team['introdcution'] = json_response['properties']['description']
        team['type'] = json_response['properties']['category']
        team['since'] = json_response['properties']['createdDate']
        team['total_loan_amount'] = json_response['properties']['createdDate']

    def parse_team_lender(self, response):
        json_response = json.loads(response.body_as_unicode())
        team = response.meta['team']

        pages = json_response['paging']

        if pages['pages'] > 0:
            for page in xrange(1, pages['pages'] + 1):
                url = 'http://api.kivaws.org/v1/teams/%s/lenders.json?page=%s' % (team['id'], page)
                req = Request(url=url, callback=self.parse_team_lender2)
                req.meta.update({'team': team})
                yield req

    def parse_team_lender2(self, response):
        json_response = json.loads(response.body_as_unicode())
        team = response.meta['team']

        for lender in json_response['lenders']:
            if 'lender_id' in lender:
                relation = TeamMemberItem()
                relation['team_id'] = team['id']
                relation['member_id'] = lender['lender_id']
                yield relation

                yield self.make_lender(lender['lender_id'])

    def make_lender(self, lender_uid):
        url = 'http://api.kivaws.org/v1/lenders/%s.json' % lender_uid
        req = Request(url=url, callback=self.parse_lender)
        return req

    def parse_lender(self, response):
        """
        通过API取得会员信息
        """
        json_response = json.loads(response.body_as_unicode())

        for lender in json_response['lenders']:
            if 'name' in lender:
                item = MemberItem()
                item['id'] = lender['lender_id']
                item['name'] = lender['name']
                item['whereabouts'] = lender['whereabouts']
                item['i_loan_because'] = lender['loan_because']
                item['join_kiva'] = lender['member_since']
                yield item
