# coding: utf-8
import json
from scrapy import Request
from scrapy.spiders import Spider
from apps.kiva.models import Team, TeamMember, Member
from crawler.items import LoanItem, PartnerItem, TeamItem, MemberItem, LoanPartnerItem, LoanTeamItem, LoanMemberItem, TeamMemberItem


class AllTeamsLendersSpider(Spider):
    """
    抓取所有Team的所有Lenders信息,只做关联和在Lenders表里添加新的抓取
    """
    name = 'AllTeamsLendersSpider'
    allowed_domains = ['kiva.org', 'api.kivaws.org']

    def start_requests(self):
        for team in Team.objects.all():
            yield self.make_team(team.id, team.uid)

    def make_team(self, team_id, team_uid):
        url = 'http://api.kivaws.org/v1/teams/%s/lenders.json' % team_id
        req = Request(url=url, callback=self.parse_team_lender)
        req.meta.update({'team_id': team_id})
        req.meta.update({'team_uid': team_uid})
        return req

    def parse_team_lender(self, response):
        json_response = json.loads(response.body_as_unicode())
        team_id = response.meta['team_id']
        team_uid = response.meta['team_uid']

        pages = json_response['paging']

        if pages['pages'] > 0:
            for page in xrange(1, pages['pages'] + 1):
                url = 'http://api.kivaws.org/v1/teams/%s/lenders.json?page=%s' % (team_id, page)
                req = Request(url=url, callback=self.parse_team_lender2)
                req.meta.update({'team_id': team_id})
                req.meta.update({'team_uid': team_uid})
                yield req

    def parse_team_lender2(self, response):
        json_response = json.loads(response.body_as_unicode())
        team_id = response.meta['team_id']
        team_uid = response.meta['team_uid']

        for lender in json_response['lenders']:
            if 'lender_id' in lender:
                relation = TeamMemberItem()
                relation['team_uid'] = team_uid
                relation['member_uid'] = lender['uid']
                relation['team_join_date'] = lender['team_join_date']
                yield relation

                member = MemberItem()
                member['uid'] = lender['uid']
                yield member
