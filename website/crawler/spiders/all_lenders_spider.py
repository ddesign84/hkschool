# coding: utf-8
from scrapy import Request, Spider
from apps.kiva.models import Member
from core.text_format import format_lines, format_number
from crawler.items import LoanItem, PartnerItem, TeamItem, MemberItem, LoanPartnerItem, LoanTeamItem, LoanMemberItem, TeamMemberItem


class AllLendersSpider(Spider):
    """
    抓取数据库中的所有name为空的Lenders

    多开进行抓取
    crawl AllTeamsSpider -a start=3000 -a end=3793 --logfile=logs/loan_teams_spider_4.log
    """
    name = 'AllLendersSpider'
    allowed_domains = ['kiva.org', 'api.kivaws.org']

    def __init__(self, page):
        self.page = int(page)

    def start_requests(self):
        count = Member.objects.filter(name=None).count()
        step = count / 10
        if self.page == 1:
            start = 1
            end = step
        elif self.page == 10:
            start = step * 9
            end = count
        else:
            start = step * (self.page - 1)
            end = start + step

        for member in Member.objects.filter(name=None).all()[start:end]:
            url = 'http://www.kiva.org/lender/%s' % member.uid
            req = Request(url=url, callback=self.parse_item)
            req.meta.update({'member_uid': member.uid})
            yield req

    def parse_item(self, response):
        member_uid = response.meta['member_uid']

        item = MemberItem()
        item['uid'] = member_uid
        item['name'] = response.xpath('//div[@id="pageHeader"]/h2/text()').extract_first().replace('Kiva Lender ', '')
        if not item['name']:
            item['name'] = member_uid
        count = len(response.xpath('//dl[@class="profileStats"]/dt').extract())
        for i in xrange(1, count + 1):
            name = response.xpath('//dl[@class="profileStats"]/dt[%s]/text()' % i).extract_first()
            value = response.xpath('//dl[@class="profileStats"]/dd[%s]/text()' % i).extract_first()
            if name == 'Location:':
                item['location'] = value
            if name == 'I loan because:':
                item['i_loan_because'] = value
            if name == 'About me:':
                item['whereabouts'] = value
            if name == 'Member Since:':
                item['join_kiva'] = value
        yield item
