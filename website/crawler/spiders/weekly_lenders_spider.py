# coding: utf-8
from scrapy import Request, Spider
from apps.kiva.models import Member, WeeklyMember
from core.text_format import format_lines, format_number
from crawler.items import LoanItem, PartnerItem, TeamItem, MemberItem, LoanPartnerItem, LoanTeamItem, LoanMemberItem, TeamMemberItem


class WeeklyLendersSpider(Spider):
    """
    抓取数据库中的所有name为空的Lenders

    多开进行抓取
    crawl WeeklyLendersSpider --logfile=logs/weekly_lenders_spider.log
    """
    name = 'WeeklyLendersSpider'
    allowed_domains = ['kiva.org', 'api.kivaws.org']

    def start_requests(self):
        for member in WeeklyMember.objects.filter(name=None).all():
            url = 'http://www.kiva.org/lender/%s' % member.uid
            req = Request(url=url, callback=self.parse_item)
            req.meta.update({'member_uid': member.uid})
            yield req

    def parse_item(self, response):
        member_uid = response.meta['member_uid']

        item = MemberItem()
        item['uid'] = member_uid
        item['name'] = response.xpath('//div[@id="pageHeader"]/h2/text()').extract_first().replace('Kiva Lender ', '')
        if not item['name']:
            item['name'] = member_uid
        count = len(response.xpath('//dl[@class="profileStats"]/dt').extract())
        for i in xrange(1, count + 1):
            name = response.xpath('//dl[@class="profileStats"]/dt[%s]/text()' % i).extract_first()
            value = response.xpath('//dl[@class="profileStats"]/dd[%s]/text()' % i).extract_first()
            if name == 'Location:':
                item['location'] = value
            if name == 'I loan because:':
                item['i_loan_because'] = value
            if name == 'About me:':
                item['whereabouts'] = value
            if name == 'Member Since:':
                item['join_kiva'] = value
        yield item
