# coding: utf-8
from scrapy import Request, Spider
from apps.kiva.models import Team
from core.text_format import format_lines, format_number
from crawler.items import LoanItem, PartnerItem, TeamItem, MemberItem, LoanPartnerItem, LoanTeamItem, LoanMemberItem, TeamMemberItem


class AllTeamsSpider(Spider):
    """
    抓取所有Team信息

    多开进行抓取
    scrapy crawl AllTeamsSpider -a page=1 --logfile=logs/all_teams_spider1.log &
    scrapy crawl AllTeamsSpider -a page=2 --logfile=logs/all_teams_spider2.log &
    scrapy crawl AllTeamsSpider -a page=3 --logfile=logs/all_teams_spider3.log &
    scrapy crawl AllTeamsSpider -a page=4 --logfile=logs/all_teams_spider4.log &
    scrapy crawl AllTeamsSpider -a page=5 --logfile=logs/all_teams_spider5.log &
    scrapy crawl AllTeamsSpider -a page=6 --logfile=logs/all_teams_spider6.log &
    scrapy crawl AllTeamsSpider -a page=7 --logfile=logs/all_teams_spider7.log &
    scrapy crawl AllTeamsSpider -a page=8 --logfile=logs/all_teams_spider8.log &
    """
    name = 'AllTeamsSpider'
    allowed_domains = ['kiva.org', 'api.kivaws.org']

    def __init__(self, page):
        self.page = int(page)

    def start_requests(self):
        count = 3800
        step = count / 8
        if self.page == 1:
            start = 0
            end = step + 1
        elif self.page == 8:
            start = step * 7
            end = count
        else:
            start = step * (self.page - 1)
            end = start + step + 1

        for page in xrange(int(start), int(end)):
            url = 'http://www.kiva.org/teams?queryString=&category=all&membershipType=all&startDate=&endDate=&userId=&sortBy=newest&pageID=%s' % page
            yield Request(url=url, callback=self.parse_list)
        
    def parse_list(self, response):
        """
        抓取贷款
        """
        print response.url
        for index, article in enumerate(response.xpath('//article[@class="teamCard listing expanded horizontal"]')):
            uid = article.xpath('h1/a/@href').re('http://www.kiva.org/team/([a-zA-Z0-9_]+)')
            id = article.xpath('div[@class="join"]/a/@href').re('team_id=(\d+)&.+')
            if not id:
                id = article.xpath('div/div[@class="join"]/a/@href').re('team_id=(\d+)&.+')

            item = TeamItem()
            item['id'] = id[0]
            item['uid'] = uid[0]

            url = 'http://www.kiva.org/team/%s' % uid[0]
            req = Request(url=url, callback=self.parse_item)
            req.meta.update({'team': item})
            yield req

    def parse_item(self, response):
        item = response.meta['team']

        item['name'] = response.xpath('//h2[@class="hPage"]/text()').extract_first().replace('Kiva Lending Team: ', '')
        item['introdcution'] = format_lines(response.xpath('//div[@class="cF profileStats g6"]//text()').extract())
        item['type'] = response.xpath('//div[@class="meta"]/a/text()').extract_first()
        item['since'] = response.xpath('//div[@class="meta"]/text()').extract()[1].replace(' team since ', '').replace('\n', '').strip()
        item['total_loan_amount'] = format_number(response.xpath('//div[@class="rightCol g4 z"]/section/div[2]/div[1]/text()').extract_first())
        item['total_number_of_member'] = format_number(response.xpath('//div[@class="rightCol g4 z"]/section/div[1]/div[1]/text()').extract_first())
        item['total_number_of_loan'] = format_number(response.xpath('//div[@class="rightCol g4 z"]/section/div[3]/div[1]/text()').extract_first())
        team_ranking = response.xpath('//div[@class="rightCol g4 z"]/section').extract()
        if len(team_ranking) > 2:
            item['team_ranking'] = '%s\n%s' % (
                format_lines(response.xpath('//div[@class="rightCol g4 z"]/section[2]//text()').extract()),
                format_lines(response.xpath('//div[@class="rightCol g4 z"]/section[3]//text()').extract())
            )
        elif len(team_ranking) > 1:
            item['team_ranking'] = format_lines(response.xpath('//div[@class="rightCol g4 z"]/section[2]//text()').extract())
        yield item
