# coding: utf-8
import json
from scrapy import Request, Spider, FormRequest
from apps.kiva.models import Member, Loan, LoanMember, TeamMember
from core.text_format import format_lines, format_number
from crawler.items import LoanItem, PartnerItem, TeamItem, MemberItem, LoanPartnerItem, LoanTeamItem, LoanMemberItem, TeamMemberItem


class AllLoansSpider(Spider):
    """
    抓取数据库中的所有name为空的Loans

    多开进行抓取
    crawl AllLoansSpider -a page=1 --logfile=logs/all_loans_spider_1.log
    """
    name = 'AllLoansSpider'
    allowed_domains = ['kiva.org', 'api.kivaws.org']

    def __init__(self, page):
        self.page = int(page)

    def start_requests(self):
        count = Loan.objects.filter(ver=1).count()
        step = count / 10
        if self.page == 1:
            start = 0
            end = step
        elif self.page == 10:
            start = step * 9
            end = count
        else:
            start = step * (self.page - 1)
            end = start + step

        i = 0
        for loan in Loan.objects.filter(ver=1).all().values('id')[start:end]:
            i += 1
            url = 'http://www.kiva.org/lend/%s' % loan['id']
            print '%s - %s' % (i, url)
            req = Request(url=url, callback=self.parse_item)
            req.meta.update({'loan_id': loan['id']})
            yield req

    def parse_item(self, response):
        loan_id = response.meta['loan_id']

        scripts = response.xpath('//script/text()').extract()
        for script in scripts:
            if 'var Loan_LoanFigureSubView = ' in script:
                api_text = script.replace('var Loan_LoanFigureSubView = ', '')[:-2]

        api = json.loads(api_text)

        item = LoanItem()
        item['id'] = api['loan']['id']
        item['loan_name'] = api['loan']['name']
        if len(api['loan']['borrowers']) > 1:
            item['group_user_names'] = ','.join([borrower['firstName'] for borrower in api['loan']['borrowers']])
        item['country'] = api['loan']['location']['country']['name']
        item['industry'] = api['loan']['sector']['name']
        item['industry2'] = api['loan']['activity']['name']
        if 'en' in api['loan']['description']:
            item['loan_overview'] = api['loan']['description']['en'].replace('<br />', '\r\n')
        item['loan_amount'] = api['loan']['loanAmount']['amount']
        item['fundraising_status'] = api['loan']['status']
        item['repayment_term'] = response.xpath('//div[@id="loanSummary"]/dl/dd[1]/text()').extract_first()
        item['repayment_schedule'] = response.xpath('//div[@id="loanSummary"]/dl/dd[2]/text()').extract_first()
        item['pre_disbursed'] = response.xpath('//div[@id="loanSummary"]/dl/dd[3]/text()').extract_first()
        item['listed'] = response.xpath('//div[@id="loanSummary"]/dl/dd[4]/text()').extract_first()
        item['currency_exchange_loss'] = response.xpath('//div[@id="loanSummary"]/dl/dd[5]/text()').extract_first()
        item['additional_information'] = '\r\n'.join(response.xpath('//section[@id="additionalLoanInformation"]/*/text()').extract())
        translalor = response.xpath('//div[@class="byLine"]/div[2]/text()').extract()
        if len(translalor) == 2:
            item['translalor'] = translalor[1]
        item['original_language'] = response.xpath('//div[@class="byLine"]/div[1]/text()').extract_first()
        if item['original_language']:
            if 'Translated from ' in item['original_language']:
                item['original_language'] = item['original_language'].replace('Translated from ', '')
        item['loan_fund_status'] = response.xpath('//div[@class="currentFunding"]/span[1]/text()').extract_first()

        # 和partner关联
        partner_id = api['loan']['partnerId']
        relation = LoanPartnerItem()
        relation['loan_id'] = item['id']
        relation['partner_id'] = partner_id
        yield relation

        # partner = PartnerItem()
        # partner['id'] = partner_id
        # yield partner

        # 和lenders关联
        for script in scripts:
            if 'var Loan_LoanView = ' in script:
                api_lenders_text = script.replace('var Loan_LoanView = ', '')[:-2]
        api_lenders = json.loads(api_lenders_text)

        url = 'http://www.kiva.org/ajax/callLoan_TopLendersSubView'
        req = FormRequest(
            url=url,
            formdata={'biz_id': str(item['id']), 'lendersJSON': api_lenders['lendersJSON'], 'show_all_lenders': '1'},
            callback=self.parse_all_lender)
        req.meta.update({'item': item})
        yield req

        url = 'http://www.kiva.org/ajax/callLoanRepaymentsSubView?id=%s&advanced=1' % item['id']
        req = Request(url=url, callback=self.parse_item2)
        req.meta.update({'item': item})
        yield req

        url = 'http://www.kiva.org/ajax/callLoan_TopLendingTeamSubView?&show_all_teams=1&includeCssAndJs=0&biz_id=%s' % item['id']
        req = Request(url=url, callback=self.parse_all_loan)
        req.meta.update({'item': item})
        yield req

    def parse_item2(self, response):
        item = response.meta['item']

        item['repayment_schedule_all'] = response.xpath('//table[@class="dataTable"]').extract_first()

        yield item

    def parse_all_loan(self, response):
        item = response.meta['item']
        loan_urls = response.xpath('//ul[@class="teamCards loanView "]/li/article/a[@class="img img-s100 thumb "]/@href').extract()

        for loan_url in loan_urls:
            team_uid = loan_url.replace('http://www.kiva.org/team/', '')

            relation = LoanTeamItem()
            relation['loan_id'] = item['id']
            relation['team_uid'] = team_uid
            yield relation

    def parse_all_lender(self, response):
        item = response.meta['item']
        lender_urls = response.xpath('//ul[@class="lenderCards default "]/li/div/a[@class="img img-s100 thumb "]/@href').extract()

        for lender_url in lender_urls:
            member_uid = lender_url.replace('http://www.kiva.org/lender/', '')

            relation = LoanMemberItem()
            relation['loan_id'] = item['id']
            relation['member_uid'] = member_uid
            yield relation
