# coding: utf-8
import json
from scrapy import Request, Spider, FormRequest
from apps.kiva.models import Member, Loan, LoanMember, TeamMember
from core.text_format import format_lines, format_number
from crawler.items import LoanItem, PartnerItem, TeamItem, MemberItem, LoanPartnerItem, LoanTeamItem, LoanMemberItem, TeamMemberItem

class WeeklySpider(Spider):
    """
    抓取数据库中的所有name为空的Loans

    多开进行抓取
    crawl WeeklySpider -a page=64 --logfile=logs/weekly_spider.log
    """
    name = 'WeeklySpider'
    allowed_domains = ['kiva.org', 'api.kivaws.org']

    def __init__(self, page):
        self.page = int(page)

    def start_requests(self):
        for i in xrange(1, self.page + 1):
            url = 'http://www.kiva.org/lend?pageID=' + str(i) + '&perPage=100&sortBy=newest&countries%5B%5D='
            yield Request(url=url, callback=self.parse_loans)

    def parse_loans(self, response):
        for url in response.xpath('//article[@class="loanCard listing  borrowerQuickLook"]/div/a/@href').extract():
            loan_id = url.replace('http://www.kiva.org/lend/', '')
            req = Request(url=url, callback=self.parse_loan)
            req.meta.update({'loan_id': loan_id})
            yield req

    def parse_loan(self, response):
        loan_id = response.meta['loan_id']

        scripts = response.xpath('//script/text()').extract()
        for script in scripts:
            if 'var Loan_LoanFigureSubView = ' in script:
                api_text = script.replace('var Loan_LoanFigureSubView = ', '')[:-2]

        api = json.loads(api_text)

        item = LoanItem()
        item['id'] = api['loan']['id']
        item['loan_name'] = api['loan']['name']
        if len(api['loan']['borrowers']) > 1:
            item['group_user_names'] = ','.join([borrower['firstName'] for borrower in api['loan']['borrowers']])
        item['country'] = api['loan']['location']['country']['name']
        item['industry'] = api['loan']['sector']['name']
        item['industry2'] = api['loan']['activity']['name']
        if 'en' in api['loan']['description']:
            item['loan_overview'] = api['loan']['description']['en'].replace('<br />', '\r\n')
        item['loan_amount'] = api['loan']['loanAmount']['amount']
        item['fundraising_status'] = api['loan']['status']
        item['repayment_term'] = response.xpath('//div[@id="loanSummary"]/dl/dd[1]/text()').extract_first()
        item['repayment_schedule'] = response.xpath('//div[@id="loanSummary"]/dl/dd[2]/text()').extract_first()
        item['pre_disbursed'] = response.xpath('//div[@id="loanSummary"]/dl/dd[3]/text()').extract_first()
        item['listed'] = response.xpath('//div[@id="loanSummary"]/dl/dd[4]/text()').extract_first()
        item['currency_exchange_loss'] = response.xpath('//div[@id="loanSummary"]/dl/dd[5]/text()').extract_first()
        item['additional_information'] = '\r\n'.join(response.xpath('//section[@id="additionalLoanInformation"]/*/text()').extract())
        translalor = response.xpath('//div[@class="byLine"]/div[2]/text()').extract()
        if len(translalor) == 2:
            item['translalor'] = translalor[1]
        item['original_language'] = response.xpath('//div[@class="byLine"]/div[1]/text()').extract_first()
        if item['original_language']:
            if 'Translated from ' in item['original_language']:
                item['original_language'] = item['original_language'].replace('Translated from ', '')
        item['loan_fund_status'] = response.xpath('//div[@class="currentFunding"]/span[1]/text()').extract_first()

        # 和partner关联
        partner_id = api['loan']['partnerId']
        relation = LoanPartnerItem()
        relation['loan_id'] = item['id']
        relation['partner_id'] = partner_id
        yield relation

        if partner_id > 457:
            partner = PartnerItem()
            partner['id'] = partner_id
            yield partner

        # 和lenders关联
        for script in scripts:
            if 'var Loan_LoanView = ' in script:
                api_lenders_text = script.replace('var Loan_LoanView = ', '')[:-2]
        api_lenders = json.loads(api_lenders_text)

        url = 'http://www.kiva.org/ajax/callLoan_TopLendersSubView'
        req = FormRequest(
            url=url,
            formdata={'biz_id': str(item['id']), 'lendersJSON': api_lenders['lendersJSON'], 'show_all_lenders': '1'},
            callback=self.parse_all_lenders)
        req.meta.update({'item': item})
        yield req

        url = 'http://www.kiva.org/ajax/callLoanRepaymentsSubView?id=%s&advanced=1' % item['id']
        req = Request(url=url, callback=self.parse_loan2)
        req.meta.update({'item': item})
        yield req

        url = 'http://www.kiva.org/ajax/callLoan_TopLendingTeamSubView?&show_all_teams=1&includeCssAndJs=0&biz_id=%s' % item['id']
        req = Request(url=url, callback=self.parse_loan_teams)
        req.meta.update({'item': item})
        yield req

    def parse_loan2(self, response):
        item = response.meta['item']

        item['repayment_schedule_all'] = response.xpath('//table[@class="dataTable"]').extract_first()

        yield item

    def parse_loan_teams(self, response):
        item = response.meta['item']
        loan_urls = response.xpath('//ul[@class="teamCards loanView "]/li/article/a[@class="img img-s100 thumb "]/@href').extract()

        for loan_url in loan_urls:
            team_uid = loan_url.replace('http://www.kiva.org/team/', '')

            relation = LoanTeamItem()
            relation['loan_id'] = item['id']
            relation['team_uid'] = team_uid
            yield relation

            url = 'http://www.kiva.org/team/%s' % team_uid
            req = Request(url=url, callback=self.parse_loan_team)
            req.meta.update({'team_uid': team_uid})
            yield req

    def parse_loan_team(self, response):
        team_uid = response.meta['team_uid']
        item = TeamItem()
        item['uid'] = team_uid

        item['name'] = response.xpath('//h2[@class="hPage"]/text()').extract_first().replace('Kiva Lending Team: ', '')
        item['introdcution'] = format_lines(response.xpath('//div[@class="cF profileStats g6"]//text()').extract())
        item['type'] = response.xpath('//div[@class="meta"]/a/text()').extract_first()
        item['since'] = response.xpath('//div[@class="meta"]/text()').extract()[1].replace(' team since ', '').replace('\n', '').strip()
        item['total_loan_amount'] = format_number(response.xpath('//div[@class="rightCol g4 z"]/section/div[2]/div[1]/text()').extract_first())
        item['total_number_of_member'] = format_number(response.xpath('//div[@class="rightCol g4 z"]/section/div[1]/div[1]/text()').extract_first())
        item['total_number_of_loan'] = format_number(response.xpath('//div[@class="rightCol g4 z"]/section/div[3]/div[1]/text()').extract_first())
        item['loans_per_member'] = format_number(response.xpath('//div[@class="rightCol g4 z"]/section/div[4]/div[1]/text()').extract_first())
        team_ranking = response.xpath('//div[@class="rightCol g4 z"]/section').extract()
        if len(team_ranking) > 2:
            item['team_ranking'] = '%s\n%s' % (
                format_lines(response.xpath('//div[@class="rightCol g4 z"]/section[2]//text()').extract()),
                format_lines(response.xpath('//div[@class="rightCol g4 z"]/section[3]//text()').extract())
            )
        elif len(team_ranking) > 1:
            item['team_ranking'] = format_lines(response.xpath('//div[@class="rightCol g4 z"]/section[2]//text()').extract())
        yield item

    def parse_all_lenders(self, response):
        item = response.meta['item']
        lender_urls = response.xpath('//ul[@class="lenderCards default "]/li/div/a[@class="img img-s100 thumb "]/@href').extract()

        for lender_url in lender_urls:
            member_uid = lender_url.replace('http://www.kiva.org/lender/', '')

            relation = LoanMemberItem()
            relation['loan_id'] = item['id']
            relation['member_uid'] = member_uid
            yield relation

            url = 'http://www.kiva.org/lender/%s' % member_uid
            req = Request(url=url, callback=self.parse_lender)
            req.meta.update({'member_uid': member_uid})
            yield req

    def parse_lender(self, response):
        member_uid = response.meta['member_uid']

        item = MemberItem()
        item['uid'] = member_uid
        item['name'] = response.xpath('//div[@id="pageHeader"]/h2/text()').extract_first().replace('Kiva Lender ', '')
        if not item['name']:
            item['name'] = member_uid
        count = len(response.xpath('//dl[@class="profileStats"]/dt').extract())
        for i in xrange(1, count + 1):
            name = response.xpath('//dl[@class="profileStats"]/dt[%s]/text()' % i).extract_first()
            value = response.xpath('//dl[@class="profileStats"]/dd[%s]/text()' % i).extract_first()
            if name == 'Location:':
                item['location'] = value
            if name == 'I loan because:':
                item['i_loan_because'] = value
            if name == 'About me:':
                item['whereabouts'] = value
            if name == 'Member Since:':
                item['join_kiva'] = value
        yield item