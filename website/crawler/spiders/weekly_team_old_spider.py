# coding: utf-8
import json
import datetime
from scrapy import Request, Spider, FormRequest
from apps.kiva.models import Member, Loan, LoanMember, TeamMember, WeeklyLoan, \
    WeeklyTeam
from core.text_format import format_lines, format_number
from crawler.items import LoanItem, PartnerItem, TeamItem, MemberItem, LoanPartnerItem, LoanTeamItem, LoanMemberItem, TeamMemberItem

class WeeklyTeamOldSpider(Spider):
    """
    抓取数据库中的所有name为空的Loans

    多开进行抓取
    crawl WeeklyTeamOldSpider --logfile=logs/weekly_spider.log
    """
    name = 'WeeklyTeamOldSpider'
    allowed_domains = ['kiva.org', 'api.kivaws.org']

    def start_requests(self):
        date = datetime.datetime.now() + datetime.timedelta(days=-3)

        for team in WeeklyTeam.objects.filter(spider_dt__lt=date).all():
            url = 'http://www.kiva.org/team/%s' % team.uid
            req = Request(url=url, callback=self.parse_loan_team)
            req.meta.update({'team_uid': team.uid})
            yield req

    def parse_loan_team(self, response):
        team_uid = response.meta['team_uid']
        item = TeamItem()
        item['uid'] = team_uid

        item['name'] = response.xpath('//h2[@class="hPage"]/text()').extract_first().replace('Kiva Lending Team: ', '')
        item['introdcution'] = format_lines(response.xpath('//div[@class="cF profileStats g6"]//text()').extract())
        item['type'] = response.xpath('//div[@class="meta"]/a/text()').extract_first()
        item['since'] = response.xpath('//div[@class="meta"]/text()').extract()[1].replace(' team since ', '').replace('\n', '').strip()
        item['total_loan_amount'] = format_number(response.xpath('//div[@class="rightCol g4 z"]/section/div[2]/div[1]/text()').extract_first())
        item['total_number_of_member'] = format_number(response.xpath('//div[@class="rightCol g4 z"]/section/div[1]/div[1]/text()').extract_first())
        item['total_number_of_loan'] = format_number(response.xpath('//div[@class="rightCol g4 z"]/section/div[3]/div[1]/text()').extract_first())
        item['loans_per_member'] = format_number(response.xpath('//div[@class="rightCol g4 z"]/section/div[4]/div[1]/text()').extract_first())
        team_ranking = response.xpath('//div[@class="rightCol g4 z"]/section').extract()
        if len(team_ranking) > 2:
            item['team_ranking'] = '%s\n%s' % (
                format_lines(response.xpath('//div[@class="rightCol g4 z"]/section[2]//text()').extract()),
                format_lines(response.xpath('//div[@class="rightCol g4 z"]/section[3]//text()').extract())
            )
        elif len(team_ranking) > 1:
            item['team_ranking'] = format_lines(response.xpath('//div[@class="rightCol g4 z"]/section[2]//text()').extract())
        yield item