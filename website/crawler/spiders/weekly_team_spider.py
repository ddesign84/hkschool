# coding: utf-8
import json
import datetime
from scrapy import Request, Spider, FormRequest
from apps.kiva.models import Member, Loan, LoanMember, TeamMember, WeeklyTeam
from core.text_format import format_lines, format_number
from crawler.items import LoanItem, PartnerItem, TeamItem, MemberItem, LoanPartnerItem, LoanTeamItem, LoanMemberItem, TeamMemberItem

class WeeklyTeamSpider(Spider):
    """
    抓取数据库中的所有name为空的Loans

    多开进行抓取
    crawl WeeklyTeamSpider --logfile=logs/weekly_team_spider.log
    """
    name = 'WeeklyTeamSpider'
    allowed_domains = ['kiva.org', 'api.kivaws.org']
    count = 0

    def start_requests(self):
        date = datetime.datetime.now() + datetime.timedelta(days=-3)

        for team in WeeklyTeam.objects.all():
            url = 'http://api.kivaws.org/v1/teams/using_shortname/%s.json' % team.uid
            req = Request(url=url, callback=self.parse_team_id)
            req.meta.update({'team_uid': team.uid})
            yield req

    def parse_team_id(self, response):
        self.count += 1
        print 'this %s' % self.count
        team_uid = response.meta['team_uid']
        api = json.loads(response.body_as_unicode())

        team_id = api['teams'][0]['id']

        url = 'http://api.kivaws.org/v1/teams/%s/lenders.json' % team_id
        req = Request(url=url, callback=self.parse_team)
        req.meta.update({'team_id': team_id})
        req.meta.update({'team_uid': team_uid})
        yield req

    def parse_team(self, response):
        team_id = response.meta['team_id']
        team_uid = response.meta['team_uid']
        api = json.loads(response.body_as_unicode())

        current_page = int(api['paging']['page'])
        total_page = int(api['paging']['pages'])

        is_next_page = True

        date = datetime.datetime.now() + datetime.timedelta(days=-8)

        for m in api['lenders']:
            if 'team_join_date' in m:
                join_date = datetime.datetime.strptime(m['team_join_date'], '%Y-%m-%dT%H:%M:%S%fZ')

                if join_date > date:
                    relation = TeamMemberItem()
                    relation['team_uid'] = team_uid
                    relation['member_uid'] = m['lender_id']
                    relation['team_join_date'] = m['team_join_date']
                    yield relation

                    member = MemberItem()
                    member['uid'] = m['lender_id']
                    yield member
                else:
                    is_next_page = False

        if is_next_page and current_page != total_page:
            url = 'http://api.kivaws.org/v1/teams/%s/lenders.json?page=%s' % (team_id, current_page + 1)
            req = Request(url=url, callback=self.parse_team)
            req.meta.update({'team_uid': team_uid})
            yield req
