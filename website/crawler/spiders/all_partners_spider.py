# coding: utf-8
from scrapy import Request, Spider
from apps.kiva.models import Member, Partner
from core.text_format import format_lines, format_number
from crawler.items import LoanItem, PartnerItem, TeamItem, MemberItem, LoanPartnerItem, LoanTeamItem, LoanMemberItem, TeamMemberItem


class AllPartnersSpider(Spider):
    """
    抓取数据库中的所有Partner

    多开进行抓取
    crawl AllPartnersSpider --logfile=logs/all_partners_spider.log
    """
    name = 'AllPartnersSpider'
    allowed_domains = ['kiva.org', 'api.kivaws.org']

    def start_requests(self):
        for partner in Partner.objects.all():
            url = 'http://www.kiva.org/partners/%s' % partner.id
            req = Request(url=url, callback=self.parse_item)
            req.meta.update({'partner_id': partner.id})
            yield req

    def parse_item(self, response):
        partner_id = response.meta['partner_id']

        item = PartnerItem()
        item['id'] = partner_id

        item['name'] = response.xpath('//div[@class="info"]/dl/dd[1]/a/text()').extract_first()
        risk_rating = response.xpath('//div[@class="info"]/dl/dd[2]/div/@title').re('([0-9\.]+)')
        if len(risk_rating) > 0:
            item['risk_rating'] = risk_rating[0]
        else:
            item['risk_rating'] = 0
        item['time_on_kiva'] = response.xpath('//div[@class="info"]/dl/dd[3]/text()').extract_first()
        item['kiva_borrower'] = response.xpath('//div[@class="info"]/dl/dd[4]/text()').extract_first()
        item['total_loan'] = response.xpath('//div[@class="info"]/dl/dd[5]/text()').extract_first()
        item['interest_fees_are_charged'] = response.xpath('//div[@class="info"]/dl/dd[6]/text()').extract_first()
        item['average_cost_to_borrower'] = response.xpath('//div[@class="info"]/dl/dd[7]/text()').extract_first()
        item['profitability'] = response.xpath('//div[@class="info"]/dl/dd[8]/text()').extract_first()
        item['average_loan_size'] = response.xpath('//div[@class="info"]/dl/dd[9]/text()').extract_first()
        item['delinquency_rate'] = response.xpath('//div[@class="info"]/dl/dd[10]/text()').extract_first()
        item['loans_at_risk_rate'] = response.xpath('//div[@class="info"]/dl/dd[11]/text()').extract_first()
        item['default_rate'] = response.xpath('//div[@class="info"]/dl/dd[12]/text()').extract_first()
        item['currency_exchange_loss_rate'] = response.xpath('//div[@class="info"]/dl/dd[13]/text()').extract_first()

        count = len(response.xpath('//table[@id="stats-table-repayment"]/tr').extract())
        for i in xrange(1, count + 1):
            name = response.xpath('//table[@id="stats-table-repayment"]/tr[%s]/th[2]/a/text()' % i).extract_first()
            if name:
                name = name.strip()
            this = response.xpath('//table[@id="stats-table-repayment"]/tr[%s]/td[1]/text()' % i).extract_first()
            if this:
                this = this.strip()
            all = response.xpath('//table[@id="stats-table-repayment"]/tr[%s]/td[2]/text()' % i).extract_first()
            if all:
                all = all.strip()

            if name == 'Start Date On Kiva':
                item['start_date_on_kiva_this'] = this
                item['start_date_on_kiva_all'] = all
            if name == 'Total Loans':
                item['total_loans_this'] = this
                item['total_loans_all'] = all
            if name == 'Amount of raised Inactive loans':
                item['amount_of_raised_inactive_loans_this'] = this
                item['amount_of_raised_inactive_loans_all'] = all
            if name == 'Number of raised Inactive loans':
                item['number_of_raised_inactive_loans_this'] = this
                item['number_of_raised_inactive_loans_all'] = all
            if name == 'Amount of Paying Back Loans':
                item['amount_of_paying_back_loans_this'] = this
                item['amount_of_paying_back_loans_all'] = all
            if name == 'Number of Paying Back Loans':
                item['number_of_paying_back_loans_this'] = this
                item['number_of_paying_back_loans_all'] = all
            if name == 'Amount of Ended Loans':
                item['amount_of_ended_loans_this'] = this
                item['amount_of_ended_loans_all'] = all
            if name == 'Number of Ended Loans':
                item['number_of_ended_loans_this'] = this
                item['number_of_ended_loans_all'] = all
            if name == 'Delinquency Rate':
                item['delinquency_rate_this'] = this
                item['delinquency_rate_all'] = all
            if name == 'Amount in Arrears':
                item['amount_in_arrears_this'] = this
                item['amount_in_arrears_this'] = all
            if name == 'Outstanding Portfolio':
                item['outstanding_portfolio_this'] = this
                item['outstanding_portfolio_all'] = all
            if name == 'Number of Loans Delinquent':
                item['number_of_loans_delinquent_this'] = this
                item['number_of_loans_delinquent_all'] = all
            if name == 'Default Rate':
                item['default_rate_this'] = this
                item['default_rate_all'] = all
            if name == 'Amount of Ended Loans Defaulted':
                item['amount_of_ended_loans_defaulted_this'] = this
                item['amount_of_ended_loans_defaulted_all'] = all
            if name == 'Number of Ended Loans Defaulted':
                item['number_of_ended_loans_defaulted_this'] = this
                item['number_of_ended_loans_defaulted_all'] = all
            if name == 'Currency Exchange Loss Rate':
                item['currency_exchange_loss_rate_this'] = this
                item['currency_exchange_loss_rate_all'] = all
            if name == 'Amount of Currency Exchange Loss':
                item['amount_of_currency_exchange_loss_this'] = this
                item['amount_of_currency_exchange_loss_all'] = all
            if name == 'Refund Rate':
                item['refund_rate_this'] = this
                item['refund_rate_all'] = all
            if name == 'Amount of Refunded Loans':
                item['amount_of_refunded_loans_this'] = this
                item['amount_of_refunded_loans_all'] = all
            if name == 'Number of Refunded Loans':
                item['number_of_refunded_loans_this'] = this
                item['number_of_refunded_loans_all'] = all

        count = len(response.xpath('//table[@id="stats-table-loans"]/tr').extract())
        for i in xrange(1, count + 1):
            name = response.xpath('//table[@id="stats-table-loans"]/tr[%s]/th[2]/a/text()' % i).extract_first()
            if name:
                name = name.strip()
            this = response.xpath('//table[@id="stats-table-loans"]/tr[%s]/td[1]/text()' % i).extract_first()
            if this:
                this = this.strip()
            all = response.xpath('//table[@id="stats-table-loans"]/tr[%s]/td[2]/text()' % i).extract_first()
            if all:
                all = all.strip()

            if name == 'Loans to Women Borrowers':
                item['loans_to_women_borrowers_this'] = this
                item['loans_to_women_borrowers_all'] = all
            if name == 'Average Loan Size':
                item['average_loan_size_this'] = this
                item['average_loan_size_all'] = all
            if name == 'Average Individual Loan Size':
                item['average_individual_loan_size_this'] = this
                item['average_individual_loan_size_all'] = all
            if name == 'Average Group Loan Size':
                item['average_group_loan_size_this'] = this
                item['average_group_loan_size_all'] = all
            if name == 'Average number of borrowers per group':
                item['average_number_of_borrowers_per_group_this'] = this
                item['average_number_of_borrowers_per_group_all'] = all
            if name == 'Average GDP per capita (PPP) in local country':
                item['average_gdp_per_capita_ppp_in_local_country_this'] = this
                item['average_gdp_per_capita_ppp_in_local_country_all'] = all
            if name == 'Average Loan Size / GDP per capita (PPP)':
                item['average_loan_size_gdp_per_capita_ppp_this'] = this
                item['average_loan_size_gdp_per_capita_ppp_all'] = all
            if name == 'Average Time to Fund a Loan':
                item['average_time_to_fund_a_loan_this'] = this
                item['average_time_to_fund_a_loan_all'] = all
            if name == 'Average Dollars Raised Per Day Per Loan':
                item['average_dollars_raised_per_day_per_loan_this'] = this
                item['average_dollars_raised_per_day_per_loan_all'] = all
            if name == 'Average Loan Term':
                item['average_loan_term_this'] = this
                item['average_loan_term_all'] = all

        count = len(response.xpath('//table[@id="stats-table-journals"]/tr').extract())
        for i in xrange(1, count + 1):
            name = response.xpath('//table[@id="stats-table-journals"]/tr[%s]/th[2]/a/text()' % i).extract_first()
            if name:
                name = name.strip()
            this = response.xpath('//table[@id="stats-table-journals"]/tr[%s]/td[1]/text()' % i).extract_first()
            if this:
                this = this.strip()
            all = response.xpath('//table[@id="stats-table-journals"]/tr[%s]/td[2]/text()' % i).extract_first()
            if all:
                all = all.strip()

            if name == 'Total Journals':
                item['total_journals_this'] = this
                item['total_journals_all'] = all
            if name == 'Journaling Rate':
                item['journaling_rate_this'] = this
                item['journaling_rate_all'] = all
            if name == 'Average Number of Comments Per Journal':
                item['average_number_of_comments_per_journal_this'] = this
                item['average_number_of_comments_per_journal_all'] = all
            if name == 'Average Number of Recommendations Per Journal':
                item['average_number_of_recommendations_per_journal_this'] = this
                item['average_number_of_recommendations_per_journal_all'] = all

        count = len(response.xpath('//table[@id="stats-table-interest"]/tr').extract())
        for i in xrange(1, count + 1):
            name = response.xpath('//table[@id="stats-table-interest"]/tr[%s]/th[2]/a/text()' % i).extract_first()
            if name:
                name = name.strip()
            this = response.xpath('//table[@id="stats-table-interest"]/tr[%s]/td[1]/text()' % i).extract_first()
            if this:
                this = this.strip()
            median = response.xpath('//table[@id="stats-table-interest"]/tr[%s]/td[2]/text()' % i).extract_first()
            if median:
                median = median.strip()
            all = response.xpath('//table[@id="stats-table-interest"]/tr[%s]/td[3]/text()' % i).extract_first()
            if all:
                all = all.strip()

            if name == 'Average Cost to Borrower':
                item['average_cost_to_borrower_this'] = this
                item['average_cost_to_borrower_median'] = median
                item['average_cost_to_borrower_all'] = all
            if name == 'Profitability (return on assets)':
                item['profitability_return_on_assets_this'] = this
                item['profitability_return_on_assets_median'] = median
                item['profitability_return_on_assets_all'] = all
            if name == 'Average Loan Size (% of per capita income)':
                item['average_loan_size_of_per_capita_income_this'] = this
                item['average_loan_size_of_per_capita_income_median'] = median
                item['average_loan_size_of_per_capita_income_all'] = all

        yield item
