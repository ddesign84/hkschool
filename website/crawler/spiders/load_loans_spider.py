# coding: utf-8
import json
from scrapy import Request
from scrapy.conf import settings
from scrapy.spiders import Spider
from crawler.items import LoanItem, PartnerItem, TeamItem, MemberItem, LoanPartnerItem, LoanTeamItem, LoanMemberItem, TeamMemberItem


class LoadLoansSpider(Spider):
    """
    加载需要抓取的Loans
    """
    name = 'LoadLoansSpider'
    allowed_domains = ['kiva.org', 'api.kivaws.org']
    start_urls = ['http://api.kivaws.org/v1/loans/search.json?sort_by=newest&status=fundraising']

    def __init__(self):
        self.ver = settings.VER
        self.download_delay = 1

    def parse(self, response):
        """
        抓取贷款
        """
        json_response = json.loads(response.body_as_unicode())

        pages = json_response['paging']

        if pages['pages'] > 0:
            for page in xrange(pages['pages'] - 20, pages['pages'] + 1):
                url = 'http://api.kivaws.org/v1/loans/search.json?sort_by=newest&status=fundraising&page=%s' % page
                yield Request(url=url, callback=self.parse2)

    def parse2(self, response):
        """
        取得贷款列表
        """
        json_response = json.loads(response.body_as_unicode())

        for loan in json_response['loans']:
            item = LoanItem()
            item['id'] = loan['id']
            yield item
