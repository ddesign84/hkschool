# coding: utf-8
import json
from scrapy import Request
from scrapy.conf import settings
from scrapy.spiders import Spider
from apps.kiva.models import Loan
from crawler.items import LoanItem, PartnerItem, TeamItem, MemberItem, LoanPartnerItem, LoanTeamItem, LoanMemberItem, TeamMemberItem


class UpdateLoansSpider(Spider):
    """
    开始对Loans做抓取
    """
    name = 'UpdateLoansSpider'
    allowed_domains = ['kiva.org', 'api.kivaws.org']

    def __init__(self):
        self.ver = settings.VER
        self.download_delay = 0
        
    def start_requests(self):
        # 取得不是当前版本， 所有状态为 fundraising 的loans做抓取
        for loan in Loan.objects.filter(ver__ne=self.ver, fundraising_status='fundraising').all():
            yield self.make_loan(loan.id)
        # 取得当前版本， 所有名称为空的 loans 做抓取
        for loan in Loan.objects.filter(ver=self.ver, name=None).all():
            yield self.make_loan(loan.id)

    def make_loan(self, loan_id):
        url = 'http://www.kiva.org/lend/%s' % loan_id
        req = Request(url=url, callback=self.parse_loan)
        return req

    def parse_loan(self, response):
        """
        通过html取得贷款更多信息
        """
        item = response.meta['loan']
        api_text = response.xpath('//section[@id="businessProfile"]/div/script/text()').extract_first().replace('var Loan_LoanFigureSubView = ', '')[:-1]
        api = json.loads(api_text)

        item['id'] = api['loan']['id']
        item['loan_name'] = api['loan']['name']
        if len(api['loan']['borrowers']) > 1:
            item['group_user_names'] = ',' % ','.join([borrower['firstName'] for borrower in api['loan']['borrowers']])
        item['country'] = api['loan']['location']['country']['name']
        item['industry'] = api['loan']['sector']['name']
        item['industry2'] = api['loan']['activity']['name']
        item['loan_overview'] = api['loan']['description']['en'].replace('<br />', '\r\n')
        item['loan_amount'] = api['loan']['loanAmount']['amount']
        item['fundraising_status'] = api['loan']['status']
        item['repayment_term'] = response.xpath('//div[@id="loanSummary"]/dl/dd[1]/text()').extract_first()
        item['repayment_schedule'] = response.xpath('//div[@id="loanSummary"]/dl/dd[2]/text()').extract_first()
        item['pre_disbursed'] = response.xpath('//div[@id="loanSummary"]/dl/dd[3]/text()').extract_first()
        item['listed'] = response.xpath('//div[@id="loanSummary"]/dl/dd[4]/text()').extract_first()
        item['currency_exchange_loss'] = response.xpath('//div[@id="loanSummary"]/dl/dd[5]/text()').extract_first()
        item['additional_information'] = '\r\n'.join(response.xpath('//section[@id="additionalLoanInformation"]/*/text()').extract())
        translalor = response.xpath('//div[@class="byLine"]/div[2]/text()').extract()
        if len(translalor) == 2:
            item['translalor'] = translalor[1]
        item['original_language'] = response.xpath('//div[@class="byLine"]/div[1]/text()').extract_first()
        if 'Translated from ' in item['original_language']:
            item['original_language'] = item['original_language'].replace('Translated from ', '')
        item['loan_fund_status'] = response.xpath('//div[@class="currentFunding"]/span[1]/text()').extract_first()

        # 和partner关联
        partner_id = api['loan']['partnerId']
        relation = LoanPartnerItem()
        relation['loan_id'] = item['id']
        relation['partner_id'] = partner_id
        yield relation

        # 抓取partner
        partner = PartnerItem()
        partner['id'] = partner_id
        yield partner

        # 抓取teams
        url = 'http://www.kiva.org/ajax/callLoan_TopLendingTeamSubView?&biz_id=%s&show_all_teams=true' % item['id']
        req = Request(url=url, callback=self.parse_loan_teams)
        req.meta.update({'loan': item})
        yield req

        # 抓取lender
        api2_text = response.xpath('//body/script[1]/text()').extract_first().replace('var Loan_LoanView = ', '')[:-1]
        api2 = json.loads(api2_text)
        url = 'http://www.kiva.org/ajax/callLoan_TopLendersSubView?biz_id=942704&show_all_lenders=1&lendersJSON=[%s]' % ','.join(api2['lendersJSON'])
        req = Request(url=url, callback=self.parse_loan_lenders)
        req.meta.update({'loan': item})
        yield req

        yield item

    def parse_loan_teams(self, response):
        loan = response.meta['loan']
        for team_url in response.xpath('//ul/li/article/a/@href').extract():
            team_uid = team_url.replace('http://www.kiva.org/team/', '')

            relation = LoanTeamItem()
            relation['loan_id'] = loan['id']
            relation['team_uid'] = team_uid
            yield relation

            team = TeamItem()
            team['uid'] = team_uid
            yield team

    def parse_loan_lenders(self, response):
        loan = response.meta['loan']
        for lender_url in response.xpath('//ul/li/div/a/@href').extract():
            lender_uid = lender_url.replace('http://www.kiva.org/lender/', '')

            relation = LoanMemberItem()
            relation['loan_id'] = loan['id']
            relation['member_uid'] = lender_uid
            yield relation

            member = MemberItem()
            member['uid'] = lender_uid
            yield member