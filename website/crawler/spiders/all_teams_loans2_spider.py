# coding: utf-8
import json
from scrapy import Request
from scrapy.spiders import Spider
from apps.kiva.models import Team, TeamMember, Member, LoanTeam
from crawler.items import LoanItem, PartnerItem, TeamItem, MemberItem, LoanPartnerItem, LoanTeamItem, LoanMemberItem, TeamMemberItem


class AllTeamsLoans2Spider(Spider):
    """
    抓取所有Team的所有Loans信息,只做关联和在Loans表里添加新的抓取
    """
    name = 'AllTeamsLoans2Spider'
    allowed_domains = ['kiva.org', 'api.kivaws.org']
    s = 0
    e = 0
    count = 0

    def __init__(self, page):
        self.page = int(page)

    def start_requests(self):
        # 0： Team.objects.filter(total_number_of_loan__gt=0, total_number_of_loan__lt=20000).count()  1、2
        # 1： Team.objects.filter(total_number_of_loan__gt=19999).count()  3-8
        # count = Team.objects.filter(total_number_of_loan__gt=19999).count()
        count = 4378
        print count
        step = count / 8
        if self.page == 1:
            start = 0
            end = step
        elif self.page == 8:
            start = step * 7
            end = count
        else:
            start = step * (self.page - 1)
            end = start + step

        self.s = start
        self.e = end

        i = 8
        # for team in Team.objects.filter(total_number_of_loan__gt=19999).count():
        #     i += 1
        #     if i % 8 == (self.page - 1):
        #         r = self.make_team(team.id, team)
        #         if r:
        #             yield r
        team_id = 94
        team_uid = 'atheists'
        for p in xrange(1, 11066):
            print p
            url = 'http://www.kiva.org/team/%s/loans?pageID=%s' % (team_uid, p)
            req = Request(url, callback=self.parse_team_loan, headers={'Referer':'http://www.kiva.org/team/atheists/loans'})
            req.meta.update({'team_id': team_id})
            req.meta.update({'team_uid': team_uid})
            yield req

    def make_team(self, team_id, team_uid):
        url = 'http://www.kiva.org/team/%s/loans' % team_uid
        self.count += 1
        print url
        print 'start: %s / end: %s / current_count: %s' % (self.s, self.e, self.count)
        if Team.objects.get(uid=team_uid).total_number_of_loan != LoanTeam.objects.filter(team_uid=team_uid).count():
            req = Request(url=url, callback=self.parse_team_loan)
            req.meta.update({'team_id': team_id})
            req.meta.update({'team_uid': team_uid})
            return req
        return None

    def parse_team_loan(self, response):
        if '403 Forbidden' in response.body:
            print '[403] %s' % response.url
        loan_urls = response.xpath('//ul[@class="loanCards default "]/li/article/a[@class="img img-s100 thumb"]/@href').extract()
        if len(loan_urls) > 0:
            print 'success %s' % response.url
        else:
            print 'error %s' % response.url
        team_id = response.meta['team_id']
        team_uid = response.meta['team_uid']

        for loan_url in loan_urls:
            loan_id = loan_url.replace('http://www.kiva.org/lend/', '')

            relation = LoanTeamItem()
            relation['loan_id'] = loan_id
            relation['team_uid'] = team_uid
            yield relation

            item = LoanItem()
            item['id'] = loan_id
            yield item

        # for page in response.xpath('//div[@class="kvpager"]/a/@href').extract():
        #     req = Request(page, callback=self.parse_team_loan)
        #     req.meta.update({'team_id': team_id})
        #     req.meta.update({'team_uid': team_uid})
        #     yield req