# coding: utf-8
import json
from scrapy import Request
from scrapy.spiders import Spider
from apps.kiva.models import Team, TeamMember, Member
from crawler.items import LoanItem, PartnerItem, TeamItem, MemberItem, LoanPartnerItem, LoanTeamItem, LoanMemberItem, TeamMemberItem


class AllTeamsLoansSpider(Spider):
    """
    抓取所有Team的所有Loans信息,只做关联和在Loans表里添加新的抓取
    """
    name = 'AllTeamsLoansSpider'
    allowed_domains = ['kiva.org', 'api.kivaws.org']

    def start_requests(self):
        for team in Team.objects.order_by('-id').all():
            yield self.make_team(team.id, team.uid)

    def make_team(self, team_id, team_uid):
        url = 'http://api.kivaws.org/v1/teams/%s/loans.json?ids_only=true' % team_id
        req = Request(url=url, callback=self.parse_team_loan)
        req.meta.update({'team_id': team_id})
        req.meta.update({'team_uid': team_uid})
        return req

    def parse_team_loan(self, response):
        json_response = json.loads(response.body_as_unicode())
        team_id = response.meta['team_id']
        team_uid = response.meta['team_uid']

        pages = json_response['paging']

        if pages['pages'] > 0:
            for page in xrange(1, pages['pages'] + 1):
                url = 'http://api.kivaws.org/v1/teams/%s/loans.json?ids_only=true&page=%s' % (team_id, page)
                req = Request(url=url, callback=self.parse_team_loan2)
                req.meta.update({'team_id': team_id})
                req.meta.update({'team_uid': team_uid})
                yield req

    def parse_team_loan2(self, response):
        json_response = json.loads(response.body_as_unicode())
        team_id = response.meta['team_id']
        team_uid = response.meta['team_uid']

        for loan_id in json_response['loans']:
            relation = LoanTeamItem()
            relation['loan_id'] = loan_id
            relation['team_uid'] = team_uid
            yield relation

            item = LoanItem()
            item['id'] = loan_id
            yield item
