# coding: utf-8
import datetime
from config.crawler import basic as settings
from apps.kiva.models import Loan, Partner, Team, Member, LoanPartner, LoanTeam, LoanMember, TeamMember, \
    WeeklyLoan, WeeklyPartner, WeeklyTeam, WeeklyMember, WeeklyLoanPartner, \
    WeeklyLoanTeam, WeeklyLoanMember, WeeklyTeamMember
from crawler.items import LoanItem, PartnerItem, TeamItem, MemberItem, LoanPartnerItem, LoanTeamItem, LoanMemberItem, TeamMemberItem


class KivaPipeline(object):
    # 所有的
    # def process_item(self, item, spider):
    #     ver = settings.VER
    #
    #     if isinstance(item, LoanItem):
    #         try:
    #             loan = Loan.objects.get(pk=item.get('id', None))
    #         except:
    #             loan = Loan()
    #             loan.id = item.get('id', None)
    #
    #         if loan.id:
    #             loan_name = item.get('loan_name', None)
    #             if loan_name:
    #                 loan.loan_name = loan_name
    #                 loan.group_user_names = item.get('group_user_names', None)
    #                 loan.country = item.get('country', None)
    #                 loan.industry = item.get('industry', None)
    #                 loan.industry2 = item.get('industry2', None)
    #                 loan.loan_overview = item.get('loan_overview', None)
    #                 loan.original_language = item.get('original_language', None)
    #                 loan.translalor = item.get('translalor', None)
    #                 loan.additional_information = item.get('additional_information', None)
    #                 loan.invited_by = item.get('invited_by', '')
    #                 loan.loan_amount = item.get('loan_amount', None)
    #                 loan.fundraising_status = item.get('fundraising_status', None)
    #                 loan.loan_fund_status = item.get('loan_fund_status', None)
    #                 loan.repayment_term = item.get('repayment_term', None)
    #                 loan.repayment_schedule = item.get('repayment_schedule', None)
    #                 loan.pre_disbursed = datetime.datetime.strptime(item.get('pre_disbursed', None), '%b %d, %Y')
    #                 loan.listed = datetime.datetime.strptime(item.get('listed', None), '%b %d, %Y')
    #                 loan.currency_exchange_loss = item.get('currency_exchange_loss', None)
    #                 loan.repayment_schedule_all = item.get('repayment_schedule_all', None)
    #             loan.ver = ver
    #             loan.save()
    #
    #     if isinstance(item, PartnerItem):
    #         try:
    #             partner = Partner.objects.get(pk=item.get('id', None))
    #         except:
    #             partner = Partner()
    #             partner.id = item.get('id', None)
    #
    #         if partner.id:
    #             if item.get('name', None):
    #                 partner.name = item.get('name', None)
    #                 partner.risk_rating = item.get('risk_rating', 0)
    #                 partner.time_on_kiva = item.get('time_on_kiva', None)
    #                 partner.kiva_borrower = item.get('kiva_borrower', None)
    #                 partner.total_loan = item.get('total_loan', None)
    #                 partner.interest_fees_are_charged = item.get('interest_fees_are_charged', None)
    #                 partner.average_cost_to_borrower = item.get('average_cost_to_borrower', None)
    #                 partner.profitability = item.get('profitability', None)
    #                 partner.average_loan_size = item.get('average_loan_size', None)
    #                 partner.delinquency_rate = item.get('delinquency_rate', None)
    #                 partner.loans_at_risk_rate = item.get('loans_at_risk_rate', None)
    #                 partner.default_rate = item.get('default_rate', None)
    #                 partner.currency_exchange_loss_rate = item.get('currency_exchange_loss_rate', None)
    #
    #                 partner.start_date_on_kiva_this = item.get('start_date_on_kiva_this', None)
    #                 partner.start_date_on_kiva_all = item.get('start_date_on_kiva_all', None)
    #                 partner.total_loans_this = item.get('total_loans_this', None)
    #                 partner.total_loans_all = item.get('total_loans_all', None)
    #                 partner.amount_of_raised_inactive_loans_this = item.get('amount_of_raised_inactive_loans_this', None)
    #                 partner.amount_of_raised_inactive_loans_all = item.get('amount_of_raised_inactive_loans_all', None)
    #                 partner.number_of_raised_inactive_loans_this = item.get('number_of_raised_inactive_loans_this', None)
    #                 partner.number_of_raised_inactive_loans_all = item.get('number_of_raised_inactive_loans_all', None)
    #                 partner.amount_of_paying_back_loans_this = item.get('amount_of_paying_back_loans_this', None)
    #                 partner.amount_of_paying_back_loans_all = item.get('amount_of_paying_back_loans_all', None)
    #                 partner.number_of_paying_back_loans_this = item.get('number_of_paying_back_loans_this', None)
    #                 partner.number_of_paying_back_loans_all = item.get('number_of_paying_back_loans_all', None)
    #                 partner.amount_of_ended_loans_this = item.get('amount_of_ended_loans_this', None)
    #                 partner.amount_of_ended_loans_all = item.get('amount_of_ended_loans_all', None)
    #                 partner.number_of_ended_loans_this = item.get('number_of_ended_loans_this', None)
    #                 partner.number_of_ended_loans_all = item.get('number_of_ended_loans_all', None)
    #                 partner.delinquency_rate_this = item.get('delinquency_rate_this', None)
    #                 partner.delinquency_rate_all = item.get('delinquency_rate_all', None)
    #                 partner.amount_in_arrears_this = item.get('amount_in_arrears_this', None)
    #                 partner.amount_in_arrears_all = item.get('amount_in_arrears_all', None)
    #                 partner.outstanding_portfolio_this = item.get('outstanding_portfolio_this', None)
    #                 partner.outstanding_portfolio_all = item.get('outstanding_portfolio_all', None)
    #                 partner.number_of_loans_delinquent_this = item.get('number_of_loans_delinquent_this', None)
    #                 partner.number_of_loans_delinquent_all = item.get('number_of_loans_delinquent_all', None)
    #                 partner.default_rate_this = item.get('default_rate_this', None)
    #                 partner.default_rate_all = item.get('default_rate_all', None)
    #                 partner.amount_of_ended_loans_defaulted_this = item.get('amount_of_ended_loans_defaulted_this', None)
    #                 partner.amount_of_ended_loans_defaulted_all = item.get('amount_of_ended_loans_defaulted_all', None)
    #                 partner.number_of_ended_loans_defaulted_this = item.get('number_of_ended_loans_defaulted_this', None)
    #                 partner.number_of_ended_loans_defaulted_all = item.get('number_of_ended_loans_defaulted_all', None)
    #                 partner.currency_exchange_loss_rate_this = item.get('currency_exchange_loss_rate_this', None)
    #                 partner.currency_exchange_loss_rate_all = item.get('currency_exchange_loss_rate_all', None)
    #                 partner.amount_of_currency_exchange_loss_this = item.get('amount_of_currency_exchange_loss_this', None)
    #                 partner.amount_of_currency_exchange_loss_all = item.get('amount_of_currency_exchange_loss_all', None)
    #                 partner.refund_rate_this = item.get('refund_rate_this', None)
    #                 partner.refund_rate_all = item.get('refund_rate_all', None)
    #                 partner.amount_of_refunded_loans_this = item.get('amount_of_refunded_loans_this', None)
    #                 partner.amount_of_refunded_loans_all = item.get('amount_of_refunded_loans_all', None)
    #                 partner.number_of_refunded_loans_this = item.get('number_of_refunded_loans_this', None)
    #                 partner.number_of_refunded_loans_all = item.get('number_of_refunded_loans_all', None)
    #
    #                 partner.loans_to_women_borrowers_this = item.get('loans_to_women_borrowers_this', None)
    #                 partner.loans_to_women_borrowers_all = item.get('loans_to_women_borrowers_all', None)
    #                 partner.average_loan_size_this = item.get('average_loan_size_this', None)
    #                 partner.average_loan_size_all = item.get('average_loan_size_all', None)
    #                 partner.average_individual_loan_size_this = item.get('average_individual_loan_size_this', None)
    #                 partner.average_individual_loan_size_all = item.get('average_individual_loan_size_all', None)
    #                 partner.average_group_loan_size_this = item.get('average_group_loan_size_this', None)
    #                 partner.average_group_loan_size_all = item.get('average_group_loan_size_all', None)
    #                 partner.average_number_of_borrowers_per_group_this = item.get('average_number_of_borrowers_per_group_this', None)
    #                 partner.average_number_of_borrowers_per_group_all = item.get('average_number_of_borrowers_per_group_all', None)
    #                 partner.average_gdp_per_capita_ppp_in_local_country_this = item.get('average_gdp_per_capita_ppp_in_local_country_this', None)
    #                 partner.average_gdp_per_capita_ppp_in_local_country_all = item.get('average_gdp_per_capita_ppp_in_local_country_all', None)
    #                 partner.average_loan_size_gdp_per_capita_ppp_this = item.get('average_loan_size_gdp_per_capita_ppp_this', None)
    #                 partner.average_loan_size_gdp_per_capita_ppp_all = item.get('average_loan_size_gdp_per_capita_ppp_all', None)
    #                 partner.average_time_to_fund_a_loan_this = item.get('average_time_to_fund_a_loan_this', None)
    #                 partner.average_time_to_fund_a_loan_all = item.get('average_time_to_fund_a_loan_all', None)
    #                 partner.average_dollars_raised_per_day_per_loan_this = item.get('average_dollars_raised_per_day_per_loan_this', None)
    #                 partner.average_dollars_raised_per_day_per_loan_all = item.get('average_dollars_raised_per_day_per_loan_all', None)
    #                 partner.average_loan_term_this = item.get('average_loan_term_this', None)
    #                 partner.average_loan_term_all = item.get('average_loan_term_all', None)
    #
    #                 partner.total_journals_this = item.get('total_journals_this', None)
    #                 partner.total_journals_all = item.get('total_journals_all', None)
    #                 partner.journaling_rate_this = item.get('journaling_rate_this', None)
    #                 partner.journaling_rate_all = item.get('journaling_rate_all', None)
    #                 partner.average_number_of_comments_per_journal_this = item.get('average_number_of_comments_per_journal_this', None)
    #                 partner.average_number_of_comments_per_journal_all = item.get('average_number_of_comments_per_journal_all', None)
    #                 partner.average_number_of_recommendations_per_journal_this = item.get('average_number_of_recommendations_per_journal_this', None)
    #                 partner.average_number_of_recommendations_per_journal_all = item.get('average_number_of_recommendations_per_journal_all', None)
    #
    #                 partner.average_cost_to_borrower_this = item.get('average_cost_to_borrower_this', None)
    #                 partner.average_cost_to_borrower_median = item.get('average_cost_to_borrower_median', None)
    #                 partner.average_cost_to_borrower_all = item.get('average_cost_to_borrower_all', None)
    #                 partner.profitability_return_on_assets_this = item.get('profitability_return_on_assets_this', None)
    #                 partner.profitability_return_on_assets_median = item.get('profitability_return_on_assets_median', None)
    #                 partner.profitability_return_on_assets_all = item.get('profitability_return_on_assets_all', None)
    #                 partner.average_loan_size_of_per_capita_income_this = item.get('average_loan_size_of_per_capita_income_this', None)
    #                 partner.average_loan_size_of_per_capita_income_median = item.get('average_loan_size_of_per_capita_income_median', None)
    #                 partner.average_loan_size_of_per_capita_income_all = item.get('average_loan_size_of_per_capita_income_all', None)
    #
    #             partner.ver = ver
    #             partner.save()
    #
    #     if isinstance(item, TeamItem):
    #         try:
    #             team = Team.objects.get(uid=item.get('uid', None))
    #         except:
    #             team = Team()
    #             team.id = item.get('id', None)
    #             team.uid = item.get('uid', None)
    #
    #         if team.uid:
    #             team.name = item.get('name', None)
    #             team.introdcution = item.get('introdcution', None)
    #             team.type = item.get('type', None)
    #             team.since = datetime.datetime.strptime(item.get('since', None), '%b %d, %Y')
    #             team.team_ranking = item.get('team_ranking', None)
    #             team.total_loan_amount = item.get('total_loan_amount', None)
    #             team.total_number_of_member = item.get('total_number_of_member', None)
    #             team.total_number_of_loan = item.get('total_number_of_loan', None)
    #             team.ver = ver
    #             team.save()
    #
    #     if isinstance(item, MemberItem):
    #         try:
    #             member = Member.objects.get(uid=item.get('uid', None))
    #         except:
    #             member = Member()
    #             member.uid = item.get('uid', None)
    #
    #         if member.uid:
    #             if item.get('name', None):
    #                 member.name = item.get('name', None)
    #                 location = item.get('location', None)
    #                 if location and len(location) > 200:
    #                     location = location[:200]
    #                 member.location = location
    #                 member.whereabouts = item.get('whereabouts', None)
    #                 member.i_loan_because = item.get('i_loan_because', None)
    #                 member.join_kiva = datetime.datetime.strptime(item.get('join_kiva', None), '%b %d, %Y')
    #             member.ver = ver
    #             member.save()
    #
    #     if isinstance(item, LoanPartnerItem):
    #         try:
    #             loan_partner = LoanPartner.objects.get(loan_id=item.get('loan_id', None), partner_id=item.get('partner_id', None))
    #         except:
    #             loan_partner = LoanPartner()
    #             loan_partner.loan_id = item.get('loan_id', None)
    #             loan_partner.partner_id = item.get('partner_id', None)
    #             loan_partner.ver = ver
    #             loan_partner.save()
    #
    #     if isinstance(item, LoanTeamItem):
    #         try:
    #             loan_team = LoanTeam.objects.get(loan_id=item.get('loan_id', None), team_uid=item.get('team_uid', None))
    #         except:
    #             loan_team = LoanTeam()
    #             loan_team.loan_id = item.get('loan_id', None)
    #             loan_team.team_uid = item.get('team_uid', None)
    #             loan_team.ver = ver
    #             loan_team.save()
    #
    #     if isinstance(item, LoanMemberItem):
    #         try:
    #             loan_member = LoanMember.objects.get(loan_id=item.get('loan_id', None), member_uid=item.get('member_uid', None))
    #         except:
    #             loan_member = LoanMember()
    #             loan_member.loan_id = item.get('loan_id', None)
    #             loan_member.member_uid = item.get('member_uid', None)
    #             loan_member.ver = ver
    #             loan_member.save()
    #
    #     if isinstance(item, TeamMemberItem):
    #         try:
    #             team_member = TeamMember.objects.get(team_uid=item.get('team_uid', None), member_uid=item.get('member_uid', None))
    #         except:
    #             team_member = TeamMember()
    #             team_member.team_uid = item.get('team_uid', None)
    #             team_member.member_uid = item.get('member_uid', None)
    #             team_member.team_join_date = datetime.datetime.strptime(item.get('team_join_date', None), '%Y-%m-%dT%H:%M:%S%fZ')
    #             team_member.ver = ver
    #             team_member.save()
    #
    #     return item

    # 周的
    def process_item(self, item, spider):
        ver = settings.VER

        if isinstance(item, LoanItem):
            try:
                weekly_loan = WeeklyLoan.objects.get(pk=item.get('id', None))
            except:
                weekly_loan = WeeklyLoan()
                weekly_loan.id = item.get('id', None)

            if weekly_loan.id:
                loan_name = item.get('loan_name', None)
                if loan_name:
                    weekly_loan.loan_name = loan_name
                    weekly_loan.group_user_names = item.get('group_user_names', None)
                    weekly_loan.country = item.get('country', None)
                    weekly_loan.industry = item.get('industry', None)
                    weekly_loan.industry2 = item.get('industry2', None)
                    weekly_loan.loan_overview = item.get('loan_overview', None)
                    weekly_loan.original_language = item.get('original_language', None)
                    weekly_loan.translalor = item.get('translalor', None)
                    weekly_loan.additional_information = item.get('additional_information', None)
                    weekly_loan.invited_by = item.get('invited_by', '')
                    weekly_loan.loan_amount = item.get('loan_amount', None)
                    weekly_loan.fundraising_status = item.get('fundraising_status', None)
                    weekly_loan.loan_fund_status = item.get('loan_fund_status', None)
                    weekly_loan.repayment_term = item.get('repayment_term', None)
                    weekly_loan.repayment_schedule = item.get('repayment_schedule', None)
                    weekly_loan.pre_disbursed = datetime.datetime.strptime(item.get('pre_disbursed', None), '%b %d, %Y')
                    weekly_loan.listed = datetime.datetime.strptime(item.get('listed', None), '%b %d, %Y')
                    weekly_loan.currency_exchange_loss = item.get('currency_exchange_loss', None)
                    weekly_loan.repayment_schedule_all = item.get('repayment_schedule_all', None)
                weekly_loan.ver = ver
                weekly_loan.save()

        if isinstance(item, PartnerItem):
            try:
                partner = Partner.objects.get(pk=item.get('id', None))
            except:
                partner = Partner()
                partner.id = item.get('id', None)

                weekly_partner = WeeklyPartner()
                weekly_partner.id = item.get('id', None)

                if weekly_partner.id:
                    if item.get('name', None):
                        weekly_partner.name = item.get('name', None)
                        weekly_partner.risk_rating = item.get('risk_rating', 0)
                        weekly_partner.time_on_kiva = item.get('time_on_kiva', None)
                        weekly_partner.kiva_borrower = item.get('kiva_borrower', None)
                        weekly_partner.total_loan = item.get('total_loan', None)
                        weekly_partner.interest_fees_are_charged = item.get('interest_fees_are_charged', None)
                        weekly_partner.average_cost_to_borrower = item.get('average_cost_to_borrower', None)
                        weekly_partner.profitability = item.get('profitability', None)
                        weekly_partner.average_loan_size = item.get('average_loan_size', None)
                        weekly_partner.delinquency_rate = item.get('delinquency_rate', None)
                        weekly_partner.loans_at_risk_rate = item.get('loans_at_risk_rate', None)
                        weekly_partner.default_rate = item.get('default_rate', None)
                        weekly_partner.currency_exchange_loss_rate = item.get('currency_exchange_loss_rate', None)

                        weekly_partner.start_date_on_kiva_this = item.get('start_date_on_kiva_this', None)
                        weekly_partner.start_date_on_kiva_all = item.get('start_date_on_kiva_all', None)
                        weekly_partner.total_loans_this = item.get('total_loans_this', None)
                        weekly_partner.total_loans_all = item.get('total_loans_all', None)
                        weekly_partner.amount_of_raised_inactive_loans_this = item.get('amount_of_raised_inactive_loans_this', None)
                        weekly_partner.amount_of_raised_inactive_loans_all = item.get('amount_of_raised_inactive_loans_all', None)
                        weekly_partner.number_of_raised_inactive_loans_this = item.get('number_of_raised_inactive_loans_this', None)
                        weekly_partner.number_of_raised_inactive_loans_all = item.get('number_of_raised_inactive_loans_all', None)
                        weekly_partner.amount_of_paying_back_loans_this = item.get('amount_of_paying_back_loans_this', None)
                        weekly_partner.amount_of_paying_back_loans_all = item.get('amount_of_paying_back_loans_all', None)
                        weekly_partner.number_of_paying_back_loans_this = item.get('number_of_paying_back_loans_this', None)
                        weekly_partner.number_of_paying_back_loans_all = item.get('number_of_paying_back_loans_all', None)
                        weekly_partner.amount_of_ended_loans_this = item.get('amount_of_ended_loans_this', None)
                        weekly_partner.amount_of_ended_loans_all = item.get('amount_of_ended_loans_all', None)
                        weekly_partner.number_of_ended_loans_this = item.get('number_of_ended_loans_this', None)
                        weekly_partner.number_of_ended_loans_all = item.get('number_of_ended_loans_all', None)
                        weekly_partner.delinquency_rate_this = item.get('delinquency_rate_this', None)
                        weekly_partner.delinquency_rate_all = item.get('delinquency_rate_all', None)
                        weekly_partner.amount_in_arrears_this = item.get('amount_in_arrears_this', None)
                        weekly_partner.amount_in_arrears_all = item.get('amount_in_arrears_all', None)
                        weekly_partner.outstanding_portfolio_this = item.get('outstanding_portfolio_this', None)
                        weekly_partner.outstanding_portfolio_all = item.get('outstanding_portfolio_all', None)
                        weekly_partner.number_of_loans_delinquent_this = item.get('number_of_loans_delinquent_this', None)
                        weekly_partner.number_of_loans_delinquent_all = item.get('number_of_loans_delinquent_all', None)
                        weekly_partner.default_rate_this = item.get('default_rate_this', None)
                        weekly_partner.default_rate_all = item.get('default_rate_all', None)
                        weekly_partner.amount_of_ended_loans_defaulted_this = item.get('amount_of_ended_loans_defaulted_this', None)
                        weekly_partner.amount_of_ended_loans_defaulted_all = item.get('amount_of_ended_loans_defaulted_all', None)
                        weekly_partner.number_of_ended_loans_defaulted_this = item.get('number_of_ended_loans_defaulted_this', None)
                        weekly_partner.number_of_ended_loans_defaulted_all = item.get('number_of_ended_loans_defaulted_all', None)
                        weekly_partner.currency_exchange_loss_rate_this = item.get('currency_exchange_loss_rate_this', None)
                        weekly_partner.currency_exchange_loss_rate_all = item.get('currency_exchange_loss_rate_all', None)
                        weekly_partner.amount_of_currency_exchange_loss_this = item.get('amount_of_currency_exchange_loss_this', None)
                        weekly_partner.amount_of_currency_exchange_loss_all = item.get('amount_of_currency_exchange_loss_all', None)
                        weekly_partner.refund_rate_this = item.get('refund_rate_this', None)
                        weekly_partner.refund_rate_all = item.get('refund_rate_all', None)
                        weekly_partner.amount_of_refunded_loans_this = item.get('amount_of_refunded_loans_this', None)
                        weekly_partner.amount_of_refunded_loans_all = item.get('amount_of_refunded_loans_all', None)
                        weekly_partner.number_of_refunded_loans_this = item.get('number_of_refunded_loans_this', None)
                        weekly_partner.number_of_refunded_loans_all = item.get('number_of_refunded_loans_all', None)

                        weekly_partner.loans_to_women_borrowers_this = item.get('loans_to_women_borrowers_this', None)
                        weekly_partner.loans_to_women_borrowers_all = item.get('loans_to_women_borrowers_all', None)
                        weekly_partner.average_loan_size_this = item.get('average_loan_size_this', None)
                        weekly_partner.average_loan_size_all = item.get('average_loan_size_all', None)
                        weekly_partner.average_individual_loan_size_this = item.get('average_individual_loan_size_this', None)
                        weekly_partner.average_individual_loan_size_all = item.get('average_individual_loan_size_all', None)
                        weekly_partner.average_group_loan_size_this = item.get('average_group_loan_size_this', None)
                        weekly_partner.average_group_loan_size_all = item.get('average_group_loan_size_all', None)
                        weekly_partner.average_number_of_borrowers_per_group_this = item.get('average_number_of_borrowers_per_group_this', None)
                        weekly_partner.average_number_of_borrowers_per_group_all = item.get('average_number_of_borrowers_per_group_all', None)
                        weekly_partner.average_gdp_per_capita_ppp_in_local_country_this = item.get('average_gdp_per_capita_ppp_in_local_country_this', None)
                        weekly_partner.average_gdp_per_capita_ppp_in_local_country_all = item.get('average_gdp_per_capita_ppp_in_local_country_all', None)
                        weekly_partner.average_loan_size_gdp_per_capita_ppp_this = item.get('average_loan_size_gdp_per_capita_ppp_this', None)
                        weekly_partner.average_loan_size_gdp_per_capita_ppp_all = item.get('average_loan_size_gdp_per_capita_ppp_all', None)
                        weekly_partner.average_time_to_fund_a_loan_this = item.get('average_time_to_fund_a_loan_this', None)
                        weekly_partner.average_time_to_fund_a_loan_all = item.get('average_time_to_fund_a_loan_all', None)
                        weekly_partner.average_dollars_raised_per_day_per_loan_this = item.get('average_dollars_raised_per_day_per_loan_this', None)
                        weekly_partner.average_dollars_raised_per_day_per_loan_all = item.get('average_dollars_raised_per_day_per_loan_all', None)
                        weekly_partner.average_loan_term_this = item.get('average_loan_term_this', None)
                        weekly_partner.average_loan_term_all = item.get('average_loan_term_all', None)

                        weekly_partner.total_journals_this = item.get('total_journals_this', None)
                        weekly_partner.total_journals_all = item.get('total_journals_all', None)
                        weekly_partner.journaling_rate_this = item.get('journaling_rate_this', None)
                        weekly_partner.journaling_rate_all = item.get('journaling_rate_all', None)
                        weekly_partner.average_number_of_comments_per_journal_this = item.get('average_number_of_comments_per_journal_this', None)
                        weekly_partner.average_number_of_comments_per_journal_all = item.get('average_number_of_comments_per_journal_all', None)
                        weekly_partner.average_number_of_recommendations_per_journal_this = item.get('average_number_of_recommendations_per_journal_this', None)
                        weekly_partner.average_number_of_recommendations_per_journal_all = item.get('average_number_of_recommendations_per_journal_all', None)

                        weekly_partner.average_cost_to_borrower_this = item.get('average_cost_to_borrower_this', None)
                        weekly_partner.average_cost_to_borrower_median = item.get('average_cost_to_borrower_median', None)
                        weekly_partner.average_cost_to_borrower_all = item.get('average_cost_to_borrower_all', None)
                        weekly_partner.profitability_return_on_assets_this = item.get('profitability_return_on_assets_this', None)
                        weekly_partner.profitability_return_on_assets_median = item.get('profitability_return_on_assets_median', None)
                        weekly_partner.profitability_return_on_assets_all = item.get('profitability_return_on_assets_all', None)
                        weekly_partner.average_loan_size_of_per_capita_income_this = item.get('average_loan_size_of_per_capita_income_this', None)
                        weekly_partner.average_loan_size_of_per_capita_income_median = item.get('average_loan_size_of_per_capita_income_median', None)
                        weekly_partner.average_loan_size_of_per_capita_income_all = item.get('average_loan_size_of_per_capita_income_all', None)

                    weekly_partner.ver = ver
                    weekly_partner.save()

        if isinstance(item, TeamItem):
            try:
                weekly_team = WeeklyTeam.objects.get(uid=item.get('uid', None))
            except:
                weekly_team = WeeklyTeam()
                weekly_team.uid = item.get('uid', None)

            if weekly_team.uid:
                weekly_team.name = item.get('name', None)
                weekly_team.introdcution = item.get('introdcution', None)
                weekly_team.type = item.get('type', None)
                weekly_team.since = datetime.datetime.strptime(item.get('since', None), '%b %d, %Y')
                weekly_team.team_ranking = item.get('team_ranking', None)
                weekly_team.total_loan_amount = item.get('total_loan_amount', None)
                weekly_team.total_number_of_member = item.get('total_number_of_member', None)
                weekly_team.total_number_of_loan = item.get('total_number_of_loan', None)
                weekly_team.loans_per_member = item.get('loans_per_member', None)
                weekly_team.ver = ver
                weekly_team.save()

        if isinstance(item, MemberItem):
            # try:
            #     member = Member.objects.get(uid=item.get('uid', None))
            # except:
            #     member = Member()
            #     member.uid = item.get('uid', None)

            try:
                weekly_member = WeeklyMember.objects.get(uid=item.get('uid', None))
            except:
                weekly_member = WeeklyMember()
                weekly_member.uid = item.get('uid', None)

            if weekly_member.uid:
                if item.get('name', None):
                    weekly_member.name = item.get('name', None)
                    location = item.get('location', None)
                    if location and len(location) > 200:
                        location = location[:200]
                    weekly_member.location = location
                    weekly_member.whereabouts = item.get('whereabouts', None)
                    weekly_member.i_loan_because = item.get('i_loan_because', None)
                    weekly_member.join_kiva = datetime.datetime.strptime(item.get('join_kiva', None), '%b %d, %Y')
                weekly_member.ver = ver
                weekly_member.save()

        if isinstance(item, LoanPartnerItem):
            try:
                weekly_loan_partner = WeeklyLoanPartner.objects.get(loan_id=item.get('loan_id', None), partner_id=item.get('partner_id', None))
            except:
                weekly_loan_partner = WeeklyLoanPartner()
                weekly_loan_partner.loan_id = item.get('loan_id', None)
                weekly_loan_partner.partner_id = item.get('partner_id', None)
                weekly_loan_partner.ver = ver
                weekly_loan_partner.save()

        if isinstance(item, LoanTeamItem):
            try:
                weekly_loan_team = WeeklyLoanTeam.objects.get(loan_id=item.get('loan_id', None), team_uid=item.get('team_uid', None))
            except:
                weekly_loan_team = WeeklyLoanTeam()
                weekly_loan_team.loan_id = item.get('loan_id', None)
                weekly_loan_team.team_uid = item.get('team_uid', None)
                weekly_loan_team.ver = ver
                weekly_loan_team.save()

        if isinstance(item, LoanMemberItem):
            try:
                weekly_loan_member = WeeklyLoanMember.objects.get(loan_id=item.get('loan_id', None), member_uid=item.get('member_uid', None))
            except:
                weekly_loan_member = WeeklyLoanMember()
                weekly_loan_member.loan_id = item.get('loan_id', None)
                weekly_loan_member.member_uid = item.get('member_uid', None)
                weekly_loan_member.ver = ver
                weekly_loan_member.save()

        if isinstance(item, TeamMemberItem):
            # try:
            #     team_member = TeamMember.objects.get(team_uid=item.get('team_uid', None), member_uid=item.get('member_uid', None))
            # except:
            #     team_member = TeamMember()
            #     team_member.team_uid = item.get('team_uid', None)
            #     team_member.member_uid = item.get('member_uid', None)
            #     team_member.team_join_date = datetime.datetime.strptime(item.get('team_join_date', None), '%Y-%m-%dT%H:%M:%S%fZ')
            #     team_member.ver = ver
            #     team_member.save()

            try:
                weekly_team_member = WeeklyTeamMember.objects.get(team_uid=item.get('team_uid', None), member_uid=item.get('member_uid', None))
            except:
                weekly_team_member = WeeklyTeamMember()
                weekly_team_member.team_uid = item.get('team_uid', None)
                weekly_team_member.member_uid = item.get('member_uid', None)
                weekly_team_member.team_join_date = datetime.datetime.strptime(item.get('team_join_date', None), '%Y-%m-%dT%H:%M:%S%fZ')
                weekly_team_member.ver = ver
                weekly_team_member.save()

        return item