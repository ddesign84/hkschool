# coding: utf-8
import random


class RandomUserAgentMiddleware(object):
    def __init__(self, user_agents):
        self.user_agents = user_agents

    @classmethod
    def from_crawler(cls, crawler):
        o = cls(crawler.settings['USER_AGENTS'])
        return o

    def process_request(self, request, spider):
        if not [key for key in request.headers if key.lower() == 'user-agent']:
            ua = random.choice(self.user_agents)
            request.headers.setdefault('User-Agent', ua)