# coding: utf-8
import MySQLdb
import re

# 配置
# Team表名
TEAM_TABLE_NAME = 'kiva_weeklyteam'

conn = MySQLdb.connect(host='localhost', port=3306, user='root', passwd='',
                       db='hkschool', charset="utf8")
cur = conn.cursor()
cur.execute('SELECT id, team_ranking FROM %s WHERE team_ranking IS NOT NULL' % TEAM_TABLE_NAME)
teams = cur.fetchall()
columns = []

COLUMN_REGEX = re.compile(r'#(\d+) for (.+) \(.+')

for team in teams:
    team_ranking = team[1]

    prefix = None
    for row in team_ranking.split('\n'):
        if row.startswith('#'):
            m = COLUMN_REGEX.match(row)
            column = '%s_%s' % (prefix, m.group(2).replace(' ', '_').replace('-', '_'))
            column = column.lower()
            if column:
                if column not in columns:
                    columns.append(column)
        else:
            prefix = row.replace(' ', '_').replace('/', '_').replace('-', '_').lower().replace('rankings_', '')

# 创建表
create_table_sql = '''CREATE TABLE kiva_weeklyteam_ranking (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  %s
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=781 DEFAULT CHARSET=utf8;
''' % '\n'.join(
    [
        '`%s_all` int(11) NOT NULL DEFAULT 0,`%s_this` int(11) NOT NULL DEFAULT 0,`%s_last` int(11) NOT NULL DEFAULT 0,' % (column, column, column) for column in columns
    ]
)
cur.execute(create_table_sql)

# 插入数据
for team in teams:
    team_ranking = team[1]

    columns = {}
    prefix = None
    for row in team_ranking.split('\n'):
        if row.startswith('#'):
            m = COLUMN_REGEX.match(row)
            column = '%s_%s' % (prefix, m.group(2).replace(' ', '_').replace('-', '_'))
            column = column.lower()
            if column:
                if column not in columns.keys():
                    if '(All Time)' in row:
                        columns['%s_all' % column] = m.group(1)
                    elif '(This Month)' in row:
                        columns['%s_this' % column] = m.group(1)
                    elif '(Last Month)' in row:
                        columns['%s_last' % column] = m.group(1)
        else:
            prefix = row.replace(' ', '_').replace('/', '_').replace('-', '_').lower().replace('rankings_', '')

    insert_sql = 'INSERT INTO kiva_weeklyteam_ranking(id, %s) VALUES (%s,%s);' % (
        ','.join([key for key in columns.keys()]),
        team[0],
        ','.join([val for val in columns.values()])
    )
    cur.execute(insert_sql)

conn.commit()

