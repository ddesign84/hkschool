# coding: utf-8
def format_lines(lines, sp='\n'):
    texts = []
    for line in lines:
        line = line.replace('\t', '').replace('\r', '').replace('\n', '')
        if line:
            texts.append(line)
    return sp.join(texts)


def format_number(price):
    return price.replace('$', '').replace(',', '')