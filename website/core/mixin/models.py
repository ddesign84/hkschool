# coding: utf-8
from django.db import models


class CreateUpdateDateMixin(models.Model):
    """
    创建时间/修改时间
    """
    create_date = models.DateTimeField('创建时间', auto_now_add=True)
    update_date = models.DateTimeField('修改时间', auto_now=True)

    class Meta:
        abstract = True
