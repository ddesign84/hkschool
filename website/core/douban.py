# coding: utf-8
import json
import urllib2
import re


PRICE_PATTERN = '(?P<price>\d+\.\d{2})'
PUBDATE_PATTERN = '(?P<year>\d{4})[-/](?P<month>\d{1,2})([-/](?P<day>\d{1,2}))?'
PAGES_PATTERN = '(?P<pages>\d+)'


def get_book_info(isbn):
    """
    取得豆瓣图书信息
    :param isbn: 图书isbn
    :return: 豆瓣图书信息
    """
    if not isbn:
        return None

    api = 'https://api.douban.com/v2/book/isbn/:%s' % isbn

    request = urllib2.urlopen(api)
    response = request.read()

    if '"msg": "book_not_found"' in response:
        return None

    return json.loads(response)


def format_price(price):
    """
    格式化豆瓣的图书价格
    1.99元 -> 1.99
    :param price:
    :return:
    """
    if not price:
        return '0.00'

    match = re.search(PRICE_PATTERN, price)
    p = match.group('price')
    if p:
        return p
    else:
        return price


def format_pubdate(pubdate):
    """
    格式化豆瓣的图书出版日期
    2014-2 -> date(2014, 2, 1)
    2014-2-1 -> date(2014, 2, 1)
    :param pubdate: 出版日期
    :return:
    """
    if not pubdate:
        return ''

    match = re.search(PUBDATE_PATTERN, pubdate)
    year = match.group('year')
    month = match.group('month')
    day = match.group('day')

    if not year or not month:
        return pubdate

    if not day:
        day = 1

    return '%s-%s-%s' % (year, month, day)


def format_pages(pages):
    """
    格式化豆瓣的图书页数
    100页 -> 100
    :param pages:
    :return:
    """
    if not pages:
        return '0'

    match = re.search(PAGES_PATTERN, pages)
    p = match.group('pages')
    if p:
        return p
    else:
        return pages
