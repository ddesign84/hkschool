# coding: utf-8
from .basic import *

# Database

DATABASES = {
    'default': {
        'NAME': 'hkschool',
        'HOST': 'localhost',
        'ENGINE': 'django.db.backends.mysql',
        'USER': 'hkschool',
        'PASSWORD': 'hkschool',
    }
}