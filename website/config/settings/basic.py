# coding: utf-8
import os

import sys

reload(sys)
sys.setdefaultencoding('utf8')

# Basic

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
SECRET_KEY = 'poa#9@0$0w5y57+b!(d_p@u0no2c!ff1dyixhp!4z#o#$6v@@o'
DEBUG = TEMPLATE_DEBUG = False
ROOT_URLCONF = 'config.urls'
WSGI_APPLICATION = 'config.wsgi.application'

# Static files (CSS, JavaScript, Images)

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/'

# STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATIC_URL = '/static/'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

# SESSION

SESSION_ENGINE = "django.contrib.sessions.backends.cache"

# Template

TEMPLATE_DIRS = (os.path.join(BASE_DIR, 'templates'),)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.tz',
    'django.contrib.messages.context_processors.messages',
    'django.core.context_processors.request',
)


# Internationalization

LANGUAGE_CODE = 'zh_cn'
TIME_ZONE = 'Asia/Shanghai'
DATE_FORMAT = 'Y-m-d'
DATETIME_FORMAT = 'Y-m-d H:i:s'
LANGUAGES = (('zh_cn', 'Simplified Chinese'),)
USE_I18N = True
USE_L10N = True
USE_TZ = False

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'config/locale'),
)

FORMAT_MODULE_PATH = 'config.locale'

# CELERY

import djcelery
djcelery.setup_loader()

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'grappelli',
    'djcelery',

    'apps.home',
    'apps.kiva',
]


MIDDLEWARE_CLASSES = [
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.locale.LocaleMiddleware',
]

LOGIN_URL = '/login/'
LOGIN_REDIRECT_URL = '/'