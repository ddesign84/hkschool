# coding: utf-8
from .basic import *

DEBUG = TEMPLATE_DEBUG = True

# Database

DATABASES = {
    'default': {
        'NAME': 'hkschool',
        'HOST': 'localhost',
        'ENGINE': 'django.db.backends.mysql',
        'USER': 'hkschool',
        'PASSWORD': 'hkschool',
    }
}

# Celery

BROKER_URL = "amqp://guest:guest@localhost:5672/"

# django-debug-tooimportlbar

INSTALLED_APPS += [
    'debug_toolbar',
]

MIDDLEWARE_CLASSES += [
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]

DEBUG_TOOLBAR_CONFIG = {
    'JQUERY_URL': 'http://cdn.staticfile.org/jquery/2.1.0/jquery.min.js'
}

DEBUG_TOOLBAR_PANELS = [
    'debug_toolbar.panels.versions.VersionsPanel',
    'debug_toolbar.panels.timer.TimerPanel',
    'debug_toolbar.panels.settings.SettingsPanel',
    'debug_toolbar.panels.headers.HeadersPanel',
    'debug_toolbar.panels.request.RequestPanel',
    'debug_toolbar.panels.sql.SQLPanel',
    'debug_toolbar.panels.staticfiles.StaticFilesPanel',
    'debug_toolbar.panels.templates.TemplatesPanel',
    'debug_toolbar.panels.cache.CachePanel',
    'debug_toolbar.panels.signals.SignalsPanel',
    'debug_toolbar.panels.logging.LoggingPanel',
    'debug_toolbar.panels.redirects.RedirectsPanel',
]