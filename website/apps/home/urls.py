# coding: utf-8
from django.conf.urls import url, patterns

from views import Index

urlpatterns = patterns('apps.home.views',
    url(r'^$', Index.as_view(), name='index'),
)