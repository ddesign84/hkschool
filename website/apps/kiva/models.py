# coding: utf-8
from django.db import models


class Loan(models.Model):
    """
    贷款
    """
    id = models.IntegerField(primary_key=True)
    loan_name = models.CharField(max_length=200, null=True, blank=True, db_index=True)
    group_user_names = models.TextField(null=True, blank=True)
    country = models.CharField(max_length=200, null=True, blank=True)
    industry = models.CharField(max_length=200, null=True, blank=True)
    industry2 = models.CharField(max_length=200, null=True, blank=True)
    loan_fund_status = models.CharField(max_length=200, null=True, blank=True)
    loan_overview = models.TextField(null=True, blank=True)
    original_language = models.CharField(max_length=200, null=True, blank=True)
    translalor = models.CharField(max_length=100, null=True, blank=True)
    additional_information = models.TextField(null=True, blank=True)
    invited_by = models.CharField(max_length=100, null=True, blank=True)
    loan_amount = models.CharField(max_length=100, null=True, blank=True)
    fundraising_status = models.CharField(max_length=100, null=True, blank=True)
    repayment_term = models.CharField(max_length=100, null=True, blank=True)
    repayment_schedule = models.CharField(max_length=100, null=True, blank=True)
    repayment_schedule_all = models.TextField(null=True, blank=True)
    pre_disbursed = models.DateField(null=True, blank=True)
    listed = models.DateField(null=True, blank=True)
    currency_exchange_loss = models.CharField(max_length=100, null=True, blank=True)
    ver = models.IntegerField()
    spider_dt = models.DateTimeField(auto_now=True)


class Partner(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=200, null=True, blank=True)
    risk_rating = models.CharField(max_length=200, null=True, blank=True)
    time_on_kiva = models.CharField(max_length=200, null=True, blank=True)
    kiva_borrower = models.CharField(max_length=200, null=True, blank=True)
    total_loan = models.CharField(max_length=200, null=True, blank=True)
    interest_fees_are_charged = models.CharField(max_length=200, null=True, blank=True)
    average_cost_to_borrower = models.CharField(max_length=200, null=True, blank=True)
    profitability = models.CharField(max_length=200, null=True, blank=True)
    average_loan_size = models.CharField(max_length=200, null=True, blank=True)
    delinquency_rate = models.CharField(max_length=200, null=True, blank=True)
    loans_at_risk_rate = models.CharField(max_length=200, null=True, blank=True)
    default_rate = models.CharField(max_length=200, null=True, blank=True)
    currency_exchange_loss_rate = models.CharField(max_length=200, null=True, blank=True)

    start_date_on_kiva_this = models.CharField(max_length=200, null=True, blank=True)
    start_date_on_kiva_all = models.CharField(max_length=200, null=True, blank=True)
    total_loans_this = models.CharField(max_length=200, null=True, blank=True)
    total_loans_all = models.CharField(max_length=200, null=True, blank=True)
    amount_of_raised_inactive_loans_this = models.CharField(max_length=200, null=True, blank=True)
    amount_of_raised_inactive_loans_all = models.CharField(max_length=200, null=True, blank=True)
    number_of_raised_inactive_loans_this = models.CharField(max_length=200, null=True, blank=True)
    number_of_raised_inactive_loans_all = models.CharField(max_length=200, null=True, blank=True)
    amount_of_paying_back_loans_this = models.CharField(max_length=200, null=True, blank=True)
    amount_of_paying_back_loans_all = models.CharField(max_length=200, null=True, blank=True)
    number_of_paying_back_loans_this = models.CharField(max_length=200, null=True, blank=True)
    number_of_paying_back_loans_all = models.CharField(max_length=200, null=True, blank=True)
    amount_of_ended_loans_this = models.CharField(max_length=200, null=True, blank=True)
    amount_of_ended_loans_all = models.CharField(max_length=200, null=True, blank=True)
    number_of_ended_loans_this = models.CharField(max_length=200, null=True, blank=True)
    number_of_ended_loans_all = models.CharField(max_length=200, null=True, blank=True)
    delinquency_rate_this = models.CharField(max_length=200, null=True, blank=True)
    delinquency_rate_all = models.CharField(max_length=200, null=True, blank=True)
    amount_in_arrears_this = models.CharField(max_length=200, null=True, blank=True)
    amount_in_arrears_all = models.CharField(max_length=200, null=True, blank=True)
    outstanding_portfolio_this = models.CharField(max_length=200, null=True, blank=True)
    outstanding_portfolio_all = models.CharField(max_length=200, null=True, blank=True)
    number_of_loans_delinquent_this = models.CharField(max_length=200, null=True, blank=True)
    number_of_loans_delinquent_all = models.CharField(max_length=200, null=True, blank=True)
    default_rate_this = models.CharField(max_length=200, null=True, blank=True)
    default_rate_all = models.CharField(max_length=200, null=True, blank=True)
    amount_of_ended_loans_defaulted_this = models.CharField(max_length=200, null=True, blank=True)
    amount_of_ended_loans_defaulted_all = models.CharField(max_length=200, null=True, blank=True)
    number_of_ended_loans_defaulted_this = models.CharField(max_length=200, null=True, blank=True)
    number_of_ended_loans_defaulted_all = models.CharField(max_length=200, null=True, blank=True)
    currency_exchange_loss_rate_this = models.CharField(max_length=200, null=True, blank=True)
    currency_exchange_loss_rate_all = models.CharField(max_length=200, null=True, blank=True)
    amount_of_currency_exchange_loss_this = models.CharField(max_length=200, null=True, blank=True)
    amount_of_currency_exchange_loss_all = models.CharField(max_length=200, null=True, blank=True)
    refund_rate_this = models.CharField(max_length=200, null=True, blank=True)
    refund_rate_all = models.CharField(max_length=200, null=True, blank=True)
    amount_of_refunded_loans_this = models.CharField(max_length=200, null=True, blank=True)
    amount_of_refunded_loans_all = models.CharField(max_length=200, null=True, blank=True)
    number_of_refunded_loans_this = models.CharField(max_length=200, null=True, blank=True)
    number_of_refunded_loans_all = models.CharField(max_length=200, null=True, blank=True)

    loans_to_women_borrowers_this = models.CharField(max_length=200, null=True, blank=True)
    loans_to_women_borrowers_all = models.CharField(max_length=200, null=True, blank=True)
    average_loan_size_this = models.CharField(max_length=200, null=True, blank=True)
    average_loan_size_all = models.CharField(max_length=200, null=True, blank=True)
    average_individual_loan_size_this = models.CharField(max_length=200, null=True, blank=True)
    average_individual_loan_size_all = models.CharField(max_length=200, null=True, blank=True)
    average_group_loan_size_this = models.CharField(max_length=200, null=True, blank=True)
    average_group_loan_size_all = models.CharField(max_length=200, null=True, blank=True)
    average_number_of_borrowers_per_group_this = models.CharField(max_length=200, null=True, blank=True)
    average_number_of_borrowers_per_group_all = models.CharField(max_length=200, null=True, blank=True)
    average_gdp_per_capita_ppp_in_local_country_this = models.CharField(max_length=200, null=True, blank=True)
    average_gdp_per_capita_ppp_in_local_country_all = models.CharField(max_length=200, null=True, blank=True)
    average_loan_size_gdp_per_capita_ppp_this = models.CharField(max_length=200, null=True, blank=True)
    average_loan_size_gdp_per_capita_ppp_all = models.CharField(max_length=200, null=True, blank=True)
    average_time_to_fund_a_loan_this = models.CharField(max_length=200, null=True, blank=True)
    average_time_to_fund_a_loan_all = models.CharField(max_length=200, null=True, blank=True)
    average_dollars_raised_per_day_per_loan_this = models.CharField(max_length=200, null=True, blank=True)
    average_dollars_raised_per_day_per_loan_all = models.CharField(max_length=200, null=True, blank=True)
    average_loan_term_this = models.CharField(max_length=200, null=True, blank=True)
    average_loan_term_all = models.CharField(max_length=200, null=True, blank=True)

    total_journals_this = models.CharField(max_length=200, null=True, blank=True)
    total_journals_all = models.CharField(max_length=200, null=True, blank=True)
    journaling_rate_this = models.CharField(max_length=200, null=True, blank=True)
    journaling_rate_all = models.CharField(max_length=200, null=True, blank=True)
    average_number_of_comments_per_journal_this = models.CharField(max_length=200, null=True, blank=True)
    average_number_of_comments_per_journal_all = models.CharField(max_length=200, null=True, blank=True)
    average_number_of_recommendations_per_journal_this = models.CharField(max_length=200, null=True, blank=True)
    average_number_of_recommendations_per_journal_all = models.CharField(max_length=200, null=True, blank=True)

    average_cost_to_borrower_this = models.CharField(max_length=200, null=True, blank=True)
    average_cost_to_borrower_median = models.CharField(max_length=200, null=True, blank=True)
    average_cost_to_borrower_all = models.CharField(max_length=200, null=True, blank=True)
    profitability_return_on_assets_this = models.CharField(max_length=200, null=True, blank=True)
    profitability_return_on_assets_median = models.CharField(max_length=200, null=True, blank=True)
    profitability_return_on_assets_all = models.CharField(max_length=200, null=True, blank=True)
    average_loan_size_of_per_capita_income_this = models.CharField(max_length=200, null=True, blank=True)
    average_loan_size_of_per_capita_income_median = models.CharField(max_length=200, null=True, blank=True)
    average_loan_size_of_per_capita_income_all = models.CharField(max_length=200, null=True, blank=True)

    ver = models.IntegerField()
    spider_dt = models.DateTimeField(auto_now=True)


class Team(models.Model):
    id = models.IntegerField(primary_key=True)
    uid = models.CharField(max_length=255, unique=True, db_index=True)
    name = models.CharField(max_length=200, null=True, blank=True)
    introdcution = models.TextField(null=True, blank=True)
    type = models.CharField(max_length=200, null=True, blank=True)
    since = models.DateField(max_length=200, null=True, blank=True)
    team_ranking = models.TextField(null=True, blank=True)
    total_loan_amount = models.CharField(max_length=200, null=True, blank=True)
    total_number_of_member = models.IntegerField(null=True, blank=True)
    total_number_of_loan = models.IntegerField(null=True, blank=True)
    loans_per_member = models.CharField(max_length=200, null=True, blank=True)
    ver = models.IntegerField()
    spider_dt = models.DateTimeField(auto_now=True)


class Member(models.Model):
    id = models.AutoField(primary_key=True)
    uid = models.CharField(max_length=200, unique=True, db_index=True)
    name = models.CharField(max_length=200, null=True, blank=True)
    location = models.CharField(max_length=200, null=True, blank=True)
    whereabouts = models.TextField(null=True, blank=True)
    i_loan_because = models.TextField(null=True, blank=True)
    join_kiva = models.DateField(null=True, blank=True)
    ver = models.IntegerField()
    spider_dt = models.DateTimeField(auto_now=True)


class LoanPartner(models.Model):
    loan_id = models.IntegerField()
    partner_id = models.IntegerField()
    ver = models.IntegerField()
    spider_dt = models.DateTimeField(auto_now=True)

    class Meta:
        index_together = ('loan_id', 'partner_id')


class LoanTeam(models.Model):
    loan_id = models.IntegerField()
    team_uid = models.CharField(max_length=200)
    ver = models.IntegerField()
    spider_dt = models.DateTimeField(auto_now=True)

    class Meta:
        index_together = ('loan_id', 'team_uid')


class LoanMember(models.Model):
    loan_id = models.IntegerField()
    member_uid = models.CharField(max_length=200)
    ver = models.IntegerField()
    spider_dt = models.DateTimeField(auto_now=True)

    class Meta:
        index_together = ('loan_id', 'member_uid')


class TeamMember(models.Model):
    team_uid = models.CharField(max_length=200)
    member_uid = models.CharField(max_length=200)
    team_join_date = models.DateTimeField()
    ver = models.IntegerField()
    spider_dt = models.DateTimeField(auto_now=True)

    class Meta:
        index_together = ('team_uid', 'member_uid')


# ------------------------------ 以下为周的

class WeeklyLoan(models.Model):
    """
    贷款
    """
    id = models.IntegerField(primary_key=True)
    loan_name = models.CharField(max_length=200, null=True, blank=True, db_index=True)
    group_user_names = models.TextField(null=True, blank=True)
    country = models.CharField(max_length=200, null=True, blank=True)
    industry = models.CharField(max_length=200, null=True, blank=True)
    industry2 = models.CharField(max_length=200, null=True, blank=True)
    loan_fund_status = models.CharField(max_length=200, null=True, blank=True)
    loan_overview = models.TextField(null=True, blank=True)
    original_language = models.CharField(max_length=200, null=True, blank=True)
    translalor = models.CharField(max_length=100, null=True, blank=True)
    additional_information = models.TextField(null=True, blank=True)
    invited_by = models.CharField(max_length=100, null=True, blank=True)
    loan_amount = models.CharField(max_length=100, null=True, blank=True)
    fundraising_status = models.CharField(max_length=100, null=True, blank=True)
    repayment_term = models.CharField(max_length=100, null=True, blank=True)
    repayment_schedule = models.CharField(max_length=100, null=True, blank=True)
    repayment_schedule_all = models.TextField(null=True, blank=True)
    pre_disbursed = models.DateField(null=True, blank=True)
    listed = models.DateField(null=True, blank=True)
    currency_exchange_loss = models.CharField(max_length=100, null=True, blank=True)
    ver = models.IntegerField()
    spider_dt = models.DateTimeField(auto_now=True)


class WeeklyPartner(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=200, null=True, blank=True)
    risk_rating = models.CharField(max_length=200, null=True, blank=True)
    time_on_kiva = models.CharField(max_length=200, null=True, blank=True)
    kiva_borrower = models.CharField(max_length=200, null=True, blank=True)
    total_loan = models.CharField(max_length=200, null=True, blank=True)
    interest_fees_are_charged = models.CharField(max_length=200, null=True, blank=True)
    average_cost_to_borrower = models.CharField(max_length=200, null=True, blank=True)
    profitability = models.CharField(max_length=200, null=True, blank=True)
    average_loan_size = models.CharField(max_length=200, null=True, blank=True)
    delinquency_rate = models.CharField(max_length=200, null=True, blank=True)
    loans_at_risk_rate = models.CharField(max_length=200, null=True, blank=True)
    default_rate = models.CharField(max_length=200, null=True, blank=True)
    currency_exchange_loss_rate = models.CharField(max_length=200, null=True, blank=True)

    start_date_on_kiva_this = models.CharField(max_length=200, null=True, blank=True)
    start_date_on_kiva_all = models.CharField(max_length=200, null=True, blank=True)
    total_loans_this = models.CharField(max_length=200, null=True, blank=True)
    total_loans_all = models.CharField(max_length=200, null=True, blank=True)
    amount_of_raised_inactive_loans_this = models.CharField(max_length=200, null=True, blank=True)
    amount_of_raised_inactive_loans_all = models.CharField(max_length=200, null=True, blank=True)
    number_of_raised_inactive_loans_this = models.CharField(max_length=200, null=True, blank=True)
    number_of_raised_inactive_loans_all = models.CharField(max_length=200, null=True, blank=True)
    amount_of_paying_back_loans_this = models.CharField(max_length=200, null=True, blank=True)
    amount_of_paying_back_loans_all = models.CharField(max_length=200, null=True, blank=True)
    number_of_paying_back_loans_this = models.CharField(max_length=200, null=True, blank=True)
    number_of_paying_back_loans_all = models.CharField(max_length=200, null=True, blank=True)
    amount_of_ended_loans_this = models.CharField(max_length=200, null=True, blank=True)
    amount_of_ended_loans_all = models.CharField(max_length=200, null=True, blank=True)
    number_of_ended_loans_this = models.CharField(max_length=200, null=True, blank=True)
    number_of_ended_loans_all = models.CharField(max_length=200, null=True, blank=True)
    delinquency_rate_this = models.CharField(max_length=200, null=True, blank=True)
    delinquency_rate_all = models.CharField(max_length=200, null=True, blank=True)
    amount_in_arrears_this = models.CharField(max_length=200, null=True, blank=True)
    amount_in_arrears_all = models.CharField(max_length=200, null=True, blank=True)
    outstanding_portfolio_this = models.CharField(max_length=200, null=True, blank=True)
    outstanding_portfolio_all = models.CharField(max_length=200, null=True, blank=True)
    number_of_loans_delinquent_this = models.CharField(max_length=200, null=True, blank=True)
    number_of_loans_delinquent_all = models.CharField(max_length=200, null=True, blank=True)
    default_rate_this = models.CharField(max_length=200, null=True, blank=True)
    default_rate_all = models.CharField(max_length=200, null=True, blank=True)
    amount_of_ended_loans_defaulted_this = models.CharField(max_length=200, null=True, blank=True)
    amount_of_ended_loans_defaulted_all = models.CharField(max_length=200, null=True, blank=True)
    number_of_ended_loans_defaulted_this = models.CharField(max_length=200, null=True, blank=True)
    number_of_ended_loans_defaulted_all = models.CharField(max_length=200, null=True, blank=True)
    currency_exchange_loss_rate_this = models.CharField(max_length=200, null=True, blank=True)
    currency_exchange_loss_rate_all = models.CharField(max_length=200, null=True, blank=True)
    amount_of_currency_exchange_loss_this = models.CharField(max_length=200, null=True, blank=True)
    amount_of_currency_exchange_loss_all = models.CharField(max_length=200, null=True, blank=True)
    refund_rate_this = models.CharField(max_length=200, null=True, blank=True)
    refund_rate_all = models.CharField(max_length=200, null=True, blank=True)
    amount_of_refunded_loans_this = models.CharField(max_length=200, null=True, blank=True)
    amount_of_refunded_loans_all = models.CharField(max_length=200, null=True, blank=True)
    number_of_refunded_loans_this = models.CharField(max_length=200, null=True, blank=True)
    number_of_refunded_loans_all = models.CharField(max_length=200, null=True, blank=True)

    loans_to_women_borrowers_this = models.CharField(max_length=200, null=True, blank=True)
    loans_to_women_borrowers_all = models.CharField(max_length=200, null=True, blank=True)
    average_loan_size_this = models.CharField(max_length=200, null=True, blank=True)
    average_loan_size_all = models.CharField(max_length=200, null=True, blank=True)
    average_individual_loan_size_this = models.CharField(max_length=200, null=True, blank=True)
    average_individual_loan_size_all = models.CharField(max_length=200, null=True, blank=True)
    average_group_loan_size_this = models.CharField(max_length=200, null=True, blank=True)
    average_group_loan_size_all = models.CharField(max_length=200, null=True, blank=True)
    average_number_of_borrowers_per_group_this = models.CharField(max_length=200, null=True, blank=True)
    average_number_of_borrowers_per_group_all = models.CharField(max_length=200, null=True, blank=True)
    average_gdp_per_capita_ppp_in_local_country_this = models.CharField(max_length=200, null=True, blank=True)
    average_gdp_per_capita_ppp_in_local_country_all = models.CharField(max_length=200, null=True, blank=True)
    average_loan_size_gdp_per_capita_ppp_this = models.CharField(max_length=200, null=True, blank=True)
    average_loan_size_gdp_per_capita_ppp_all = models.CharField(max_length=200, null=True, blank=True)
    average_time_to_fund_a_loan_this = models.CharField(max_length=200, null=True, blank=True)
    average_time_to_fund_a_loan_all = models.CharField(max_length=200, null=True, blank=True)
    average_dollars_raised_per_day_per_loan_this = models.CharField(max_length=200, null=True, blank=True)
    average_dollars_raised_per_day_per_loan_all = models.CharField(max_length=200, null=True, blank=True)
    average_loan_term_this = models.CharField(max_length=200, null=True, blank=True)
    average_loan_term_all = models.CharField(max_length=200, null=True, blank=True)

    total_journals_this = models.CharField(max_length=200, null=True, blank=True)
    total_journals_all = models.CharField(max_length=200, null=True, blank=True)
    journaling_rate_this = models.CharField(max_length=200, null=True, blank=True)
    journaling_rate_all = models.CharField(max_length=200, null=True, blank=True)
    average_number_of_comments_per_journal_this = models.CharField(max_length=200, null=True, blank=True)
    average_number_of_comments_per_journal_all = models.CharField(max_length=200, null=True, blank=True)
    average_number_of_recommendations_per_journal_this = models.CharField(max_length=200, null=True, blank=True)
    average_number_of_recommendations_per_journal_all = models.CharField(max_length=200, null=True, blank=True)

    average_cost_to_borrower_this = models.CharField(max_length=200, null=True, blank=True)
    average_cost_to_borrower_median = models.CharField(max_length=200, null=True, blank=True)
    average_cost_to_borrower_all = models.CharField(max_length=200, null=True, blank=True)
    profitability_return_on_assets_this = models.CharField(max_length=200, null=True, blank=True)
    profitability_return_on_assets_median = models.CharField(max_length=200, null=True, blank=True)
    profitability_return_on_assets_all = models.CharField(max_length=200, null=True, blank=True)
    average_loan_size_of_per_capita_income_this = models.CharField(max_length=200, null=True, blank=True)
    average_loan_size_of_per_capita_income_median = models.CharField(max_length=200, null=True, blank=True)
    average_loan_size_of_per_capita_income_all = models.CharField(max_length=200, null=True, blank=True)

    ver = models.IntegerField()
    spider_dt = models.DateTimeField(auto_now=True)


class WeeklyTeam(models.Model):
    uid = models.CharField(max_length=255, unique=True, db_index=True)
    name = models.CharField(max_length=200, null=True, blank=True)
    introdcution = models.TextField(null=True, blank=True)
    type = models.CharField(max_length=200, null=True, blank=True)
    since = models.DateField(max_length=200, null=True, blank=True)
    team_ranking = models.TextField(null=True, blank=True)
    total_loan_amount = models.CharField(max_length=200, null=True, blank=True)
    total_number_of_member = models.IntegerField(null=True, blank=True)
    total_number_of_loan = models.IntegerField(null=True, blank=True)
    loans_per_member = models.CharField(max_length=200, null=True, blank=True)
    ver = models.IntegerField()
    spider_dt = models.DateTimeField(auto_now=True)


class WeeklyMember(models.Model):
    id = models.AutoField(primary_key=True)
    uid = models.CharField(max_length=200, unique=True, db_index=True)
    name = models.CharField(max_length=200, null=True, blank=True)
    location = models.CharField(max_length=200, null=True, blank=True)
    whereabouts = models.TextField(null=True, blank=True)
    i_loan_because = models.TextField(null=True, blank=True)
    join_kiva = models.DateField(null=True, blank=True)
    ver = models.IntegerField()
    spider_dt = models.DateTimeField(auto_now=True)


class WeeklyLoanPartner(models.Model):
    loan_id = models.IntegerField()
    partner_id = models.IntegerField()
    ver = models.IntegerField()
    spider_dt = models.DateTimeField(auto_now=True)

    class Meta:
        index_together = ('loan_id', 'partner_id')


class WeeklyLoanTeam(models.Model):
    loan_id = models.IntegerField()
    team_uid = models.CharField(max_length=200)
    ver = models.IntegerField()
    spider_dt = models.DateTimeField(auto_now=True)

    class Meta:
        index_together = ('loan_id', 'team_uid')


class WeeklyLoanMember(models.Model):
    loan_id = models.IntegerField()
    member_uid = models.CharField(max_length=200)
    ver = models.IntegerField()
    spider_dt = models.DateTimeField(auto_now=True)

    class Meta:
        index_together = ('loan_id', 'member_uid')


class WeeklyTeamMember(models.Model):
    team_uid = models.CharField(max_length=200)
    member_uid = models.CharField(max_length=200)
    team_join_date = models.DateTimeField()
    ver = models.IntegerField()
    spider_dt = models.DateTimeField(auto_now=True)

    class Meta:
        index_together = ('team_uid', 'member_uid')